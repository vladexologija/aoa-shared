module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react', 'plugin:sonarjs/recommended', 'plugin:jest/recommended'],
  plugins: ['prettier', 'jest', 'sonarjs'],
  rules: {
    'prettier/prettier': ['error'],
    'react/jsx-filename-extension': ['off'],
    'sonarjs/cognitive-complexity': ['error', 30],
    'react/jsx-props-no-spreading': ['off'],
    'import/prefer-default-export': ['off'],
    'jest/no-disabled-tests': ['off'],
    'sonarjs/no-duplicate-string': ['off'],
  },
  settings: {
    'import/resolver': {
      'babel-module': {},
    },
  },
};
