@Library('jenkins-library') _

def notifyChannel = "#c-aoa-notify"

pipeline {
  agent {
    kubernetes {
      inheritFrom ''
      defaultContainer 'node'
      yamlFile 'JenkinsKubernetesPod.yaml'
    }
  }
  options{
    timeout(time: 15, unit: 'MINUTES')
    timestamps ()
    disableConcurrentBuilds()
  }
  environment {
    GIT = credentials('ct-github')
  }
  stages {
    stage('Init') {
      steps {
        notifyBuild(notifyChannel, 'STARTED')
        sh 'yarn'
      }
    }

    stage('Test') {
      steps {
        sh 'yarn schema:dev'
        sh 'yarn test:ci'
      }
      post {
        always {
          summaryTestWithJsonFile('Tests', "reports/test-report.json")
          junit allowEmptyResults: true, testResults: "reports/test-report.xml"
        }
      }
    }

    stage('Release') {
      when { branch "master" }
      environment {
        GITHUB_TOKEN="${GIT_PSW}"
      }
      steps {
        // Run optional required steps before releasing
        sh 'yarn build'
        sh 'yarn semantic-release'
      }
    }
  }

  post {
    always {
      summaryDuration()
      // Success or failure, always send notifications
      notifyBuild(notifyChannel, currentBuild.result)
      logstashSend failBuild: false, maxLines: -1
    }
  }
}
