"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useCreateAddress = require("./useCreateAddress");

Object.keys(_useCreateAddress).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useCreateAddress[key];
    }
  });
});

var _useDeleteAddress = require("./useDeleteAddress");

Object.keys(_useDeleteAddress).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useDeleteAddress[key];
    }
  });
});

var _useGetAddresses = require("./useGetAddresses");

Object.keys(_useGetAddresses).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useGetAddresses[key];
    }
  });
});

var _useMakePrimaryAddress = require("./useMakePrimaryAddress");

Object.keys(_useMakePrimaryAddress).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useMakePrimaryAddress[key];
    }
  });
});

var _useSaveAddress = require("./useSaveAddress");

Object.keys(_useSaveAddress).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useSaveAddress[key];
    }
  });
});

var _useUpdateAddress = require("./useUpdateAddress");

Object.keys(_useUpdateAddress).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useUpdateAddress[key];
    }
  });
});