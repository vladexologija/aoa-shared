"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCreateAddress = exports.CREATE_ADDRESS_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

var _useGetAddresses = require("./useGetAddresses");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE_ADDRESS_MUTATION = (0, _client.gql)`
  mutation CreateAddress($input: CreateAddressInput!) {
    createAddress(input: $input) {
      address {
        ...AddressFields
        phoneNumber
        deliveryInstructions
        default
      }
      isValidForShipping
      candidates {
        line1
        line2
        city
        state
        zipCode
      }
    }
  }
  ${_fragments.addressFragment}
`;
exports.CREATE_ADDRESS_MUTATION = CREATE_ADDRESS_MUTATION;

const useCreateAddress = () => {
  const [createAddress, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CREATE_ADDRESS_MUTATION, {
    update(cache, {
      data: mutationResult
    }) {
      const createdAddress = (0, _get.default)(mutationResult, 'createAddress.address');

      if (createdAddress) {
        const {
          addresses: cachedAddresses
        } = cache.readQuery({
          query: _useGetAddresses.GET_ADDRESSES_QUERY
        });
        const addressArray = []; // if the created address is the primary, make sure the older primary is removed

        (cachedAddresses || []).forEach(address => {
          addressArray.push(createdAddress.default ? { ...address,
            default: false
          } : address);
        });
        addressArray.push(createdAddress);
        cache.writeQuery({
          query: _useGetAddresses.GET_ADDRESSES_QUERY,
          data: {
            addresses: addressArray
          }
        });
      }
    }

  });
  const createAddressCallback = (0, _react.useCallback)((address, skipAddressValidation = false) => {
    createAddress({
      variables: {
        input: {
          address,
          skipAddressValidation
        }
      }
    }).catch(() => null);
  }, [createAddress]);
  return {
    createAddress: createAddressCallback,
    loading,
    error,
    address: (0, _get.default)(data, 'createAddress.address'),
    isValidForShipping: (0, _get.default)(data, 'createAddress.isValidForShipping'),
    candidates: ((0, _get.default)(data, 'createAddress.candidates') || []).map(candidate => ({ ...candidate,
      id: Object.values(candidate).join()
    }))
  };
};

exports.useCreateAddress = useCreateAddress;