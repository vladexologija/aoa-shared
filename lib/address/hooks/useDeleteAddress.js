"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useDeleteAddress = exports.DELETE_ADDRESS = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _useGetAddresses = require("./useGetAddresses");

const DELETE_ADDRESS = (0, _client.gql)`
  mutation DeleteAddress($input: DeleteAddressInput!) {
    deleteAddress(input: $input)
  }
`;
exports.DELETE_ADDRESS = DELETE_ADDRESS;

const useDeleteAddress = () => {
  const [deleteAddress, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(DELETE_ADDRESS, {
    update: (cache, {
      data: mutationResult
    }) => {
      const {
        addresses
      } = cache.readQuery({
        query: _useGetAddresses.GET_ADDRESSES_QUERY
      });
      cache.writeQuery({
        query: _useGetAddresses.GET_ADDRESSES_QUERY,
        data: {
          addresses: (addresses || []).filter(address => address.id !== mutationResult.deleteAddress)
        }
      });
    }
  });
  const deleteAddressCallback = (0, _react.useCallback)(addressId => {
    deleteAddress({
      variables: {
        input: {
          addressId
        }
      }
    }).catch(() => null);
  }, [deleteAddress]);
  return {
    deleteAddress: deleteAddressCallback,
    loading,
    error,
    address: data && data.deleteAddress
  };
};

exports.useDeleteAddress = useDeleteAddress;