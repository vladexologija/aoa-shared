"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetAddresses = exports.GET_ADDRESSES_QUERY = void 0;

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GET_ADDRESSES_QUERY = (0, _client.gql)`
  query GetAddresses($customerId: ID) {
    addresses(customerId: $customerId) {
      ...AddressFields
      phoneNumber
      default
    }
  }
  ${_fragments.addressFragment}
`;
exports.GET_ADDRESSES_QUERY = GET_ADDRESSES_QUERY;

const useGetAddresses = customerId => {
  const {
    loading,
    data,
    error,
    refetch
  } = (0, _client.useQuery)(GET_ADDRESSES_QUERY, {
    variables: {
      customerId
    }
  });
  return {
    loading,
    error,
    addresses: (0, _get.default)(data, 'addresses', []),
    refetch
  };
};

exports.useGetAddresses = useGetAddresses;