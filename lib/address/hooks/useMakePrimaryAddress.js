"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useMakePrimaryAddress = exports.MAKE_PRIMARY_ADDRESS_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _useGetAddresses = require("./useGetAddresses");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const MAKE_PRIMARY_ADDRESS_MUTATION = (0, _client.gql)`
  mutation MakePrimaryAddress($input: MakeAddressPrimaryInput!) {
    makePrimaryAddress(input: $input) {
      id
    }
  }
`;
exports.MAKE_PRIMARY_ADDRESS_MUTATION = MAKE_PRIMARY_ADDRESS_MUTATION;

const useMakePrimaryAddress = () => {
  const [makePrimaryAddress, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(MAKE_PRIMARY_ADDRESS_MUTATION, {
    update: (cache, {
      data: mutationResult
    }) => {
      const primaryAddressId = (0, _get.default)(mutationResult, 'makePrimaryAddress.id');

      if (primaryAddressId) {
        const {
          addresses
        } = cache.readQuery({
          query: _useGetAddresses.GET_ADDRESSES_QUERY
        });
        cache.writeQuery({
          query: _useGetAddresses.GET_ADDRESSES_QUERY,
          data: {
            addresses: (addresses || []).map(address => ({ ...address,
              default: address.id === primaryAddressId
            }))
          }
        });
      }
    }
  });
  const makePrimaryAddressCallback = (0, _react.useCallback)(addressId => {
    makePrimaryAddress({
      variables: {
        input: {
          addressId
        }
      }
    }).catch(() => null);
  }, [makePrimaryAddress]);
  return {
    makePrimaryAddress: makePrimaryAddressCallback,
    loading,
    error,
    data: data && data.makePrimaryAddress
  };
};

exports.useMakePrimaryAddress = useMakePrimaryAddress;