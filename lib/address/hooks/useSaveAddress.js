"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSaveAddress = void 0;

var _useCreateAddress = require("./useCreateAddress");

var _useUpdateAddress = require("./useUpdateAddress");

const useSaveAddress = newAddress => {
  const {
    createAddress,
    loading: createLoading,
    error: createError,
    address: addressCreated,
    isValidForShipping: createIsValidForShipping,
    candidates: createCandidates
  } = (0, _useCreateAddress.useCreateAddress)();
  const {
    updateAddress,
    loading: updateLoading,
    error: updateError,
    address: addressUpdated,
    isValidForShipping: updateIsValidForShipping,
    candidates: updateCandidates
  } = (0, _useUpdateAddress.useUpdateAddress)();
  return newAddress ? {
    saveAddress: createAddress,
    loading: createLoading,
    error: createError,
    address: addressCreated,
    isValidForShipping: createIsValidForShipping,
    candidates: createCandidates
  } : {
    saveAddress: updateAddress,
    loading: updateLoading,
    error: updateError,
    address: addressUpdated,
    isValidForShipping: updateIsValidForShipping,
    candidates: updateCandidates
  };
};

exports.useSaveAddress = useSaveAddress;