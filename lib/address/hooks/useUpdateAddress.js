"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateAddress = exports.UPDATE_ADDRESS_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const UPDATE_ADDRESS_MUTATION = (0, _client.gql)`
  mutation UpdateAddress($input: UpdateAddressInput!) {
    updateAddress(input: $input) {
      address {
        ...AddressFields
        phoneNumber
        deliveryInstructions
        default
      }
      isValidForShipping
      candidates {
        line1
        line2
        city
        state
        zipCode
      }
    }
  }
  ${_fragments.addressFragment}
`;
exports.UPDATE_ADDRESS_MUTATION = UPDATE_ADDRESS_MUTATION;

const useUpdateAddress = () => {
  const [updateAddress, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_ADDRESS_MUTATION);
  const updateAddressCallback = (0, _react.useCallback)((address, skipAddressValidation = false) => {
    updateAddress({
      variables: {
        input: {
          address,
          skipAddressValidation
        }
      }
    }).catch(() => null);
  }, [updateAddress]);
  return {
    updateAddress: updateAddressCallback,
    loading,
    error,
    address: (0, _get.default)(data, 'updateAddress.address'),
    isValidForShipping: (0, _get.default)(data, 'updateAddress.isValidForShipping'),
    candidates: ((0, _get.default)(data, 'updateAddress.candidates') || []).map(candidate => ({ ...candidate,
      id: Object.values(candidate).join()
    }))
  };
};

exports.useUpdateAddress = useUpdateAddress;