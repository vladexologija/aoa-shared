"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useChangePassword = exports.CHANGE_PASSWORD_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const CHANGE_PASSWORD_MUTATION = (0, _client.gql)`
  mutation ChangePassword($newPassword: String!, $token: String!) {
    changePassword(newPassword: $newPassword, token: $token)
  }
`;
exports.CHANGE_PASSWORD_MUTATION = CHANGE_PASSWORD_MUTATION;

const useChangePassword = () => {
  const [changePassword, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CHANGE_PASSWORD_MUTATION);
  const changePasswordCallback = (0, _react.useCallback)((newPassword, token) => {
    changePassword({
      variables: {
        newPassword,
        token
      }
    }).catch(() => null);
  }, [changePassword]);
  return {
    changePassword: changePasswordCallback,
    loading,
    error,
    data: data && data.changePassword
  };
};

exports.useChangePassword = useChangePassword;