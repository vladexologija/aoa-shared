"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCheckResetPasswordToken = exports.CHECK_PASSWORD_RESET_TOKEN_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const CHECK_PASSWORD_RESET_TOKEN_MUTATION = (0, _client.gql)`
  mutation CheckResetPasswordToken($input: CheckTokenInput!) {
    checkResetPasswordToken(input: $input)
  }
`;
exports.CHECK_PASSWORD_RESET_TOKEN_MUTATION = CHECK_PASSWORD_RESET_TOKEN_MUTATION;

const useCheckResetPasswordToken = () => {
  const [checkResetPasswordToken, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CHECK_PASSWORD_RESET_TOKEN_MUTATION);
  const checkResetPasswordTokenCallback = (0, _react.useCallback)(token => {
    checkResetPasswordToken({
      variables: {
        input: {
          token
        }
      }
    }).catch(() => null);
  }, [checkResetPasswordToken]);
  return {
    checkResetPasswordToken: checkResetPasswordTokenCallback,
    loading,
    error,
    data: data && data.checkResetPasswordToken
  };
};

exports.useCheckResetPasswordToken = useCheckResetPasswordToken;