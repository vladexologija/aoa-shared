"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useConfirmRegisterEmail = exports.CONFIRM_REGISTER_EMAIL_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const CONFIRM_REGISTER_EMAIL_MUTATION = (0, _client.gql)`
  mutation ConfirmRegisterEmail($token: String!) {
    confirmRegisterEmail(token: $token) {
      accessToken
    }
  }
`;
exports.CONFIRM_REGISTER_EMAIL_MUTATION = CONFIRM_REGISTER_EMAIL_MUTATION;

const useConfirmRegisterEmail = () => {
  const [confirmRegisterEmail, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CONFIRM_REGISTER_EMAIL_MUTATION);
  const confirmRegisterEmailCallback = (0, _react.useCallback)(token => {
    confirmRegisterEmail({
      variables: {
        token
      }
    }).catch(() => null);
  }, [confirmRegisterEmail]);
  return {
    confirmRegisterEmail: confirmRegisterEmailCallback,
    loading,
    error,
    data: data && data.confirmRegisterEmail
  };
};

exports.useConfirmRegisterEmail = useConfirmRegisterEmail;