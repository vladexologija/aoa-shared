"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useExistsCustomerEmail = exports.CONFIRM_EXISTS_CUSTOMER_EMAIL = void 0;

var _client = require("@apollo/client");

const CONFIRM_EXISTS_CUSTOMER_EMAIL = (0, _client.gql)`
  query ExistsCustomerEmail($email: String!) {
    existsCustomerEmail(email: $email)
  }
`;
exports.CONFIRM_EXISTS_CUSTOMER_EMAIL = CONFIRM_EXISTS_CUSTOMER_EMAIL;

const useExistsCustomerEmail = email => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(CONFIRM_EXISTS_CUSTOMER_EMAIL, {
    variables: {
      email
    }
  });
  return {
    loading,
    error,
    data: data && data.existsCustomerEmail
  };
};

exports.useExistsCustomerEmail = useExistsCustomerEmail;