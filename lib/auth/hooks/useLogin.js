"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLogin = exports.LOGIN_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const LOGIN_MUTATION = (0, _client.gql)`
  mutation CustomerLogin($email: String!, $password: String!) {
    customerLogin(email: $email, password: $password) {
      accessToken
    }
  }
`;
exports.LOGIN_MUTATION = LOGIN_MUTATION;

const useLogin = () => {
  const [customerLogin, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(LOGIN_MUTATION);
  const customerLoginCallback = (0, _react.useCallback)((email, password) => {
    customerLogin({
      variables: {
        email,
        password
      }
    }).catch(() => null);
  }, [customerLogin]);
  return {
    customerLogin: customerLoginCallback,
    loading,
    error,
    data: data && data.customerLogin
  };
};

exports.useLogin = useLogin;