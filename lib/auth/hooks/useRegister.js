"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRegister = exports.REGISTER_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const REGISTER_MUTATION = (0, _client.gql)`
  mutation RegisterUser($user: CustomerRegister!) {
    customerRegister(customer: $user)
  }
`;
exports.REGISTER_MUTATION = REGISTER_MUTATION;

const useRegister = () => {
  const [customerRegister, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(REGISTER_MUTATION);
  const customerRegisterCallback = (0, _react.useCallback)((firstName, lastName, email, password, marketingOptIn, invitationCode, acceptedTerms) => {
    customerRegister({
      variables: {
        user: {
          firstName,
          lastName,
          email,
          password,
          marketingOptIn,
          invitationCode,
          acceptedTerms
        }
      }
    }).catch(() => null);
  }, [customerRegister]);
  return {
    customerRegister: customerRegisterCallback,
    loading,
    error,
    data: data && data.customerRegister
  };
};

exports.useRegister = useRegister;