"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useResetPassword = exports.RESET_PASSWORD_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const RESET_PASSWORD_MUTATION = (0, _client.gql)`
  mutation ResetPassword($email: String!) {
    resetPassword(email: $email)
  }
`;
exports.RESET_PASSWORD_MUTATION = RESET_PASSWORD_MUTATION;

const useResetPassword = () => {
  const [resetPassword, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(RESET_PASSWORD_MUTATION);
  const resetPasswordCallback = (0, _react.useCallback)(email => {
    resetPassword({
      variables: {
        email
      }
    }).catch(() => null);
  }, [resetPassword]);
  return {
    resetPassword: resetPasswordCallback,
    loading,
    error,
    data: data && data.resetPassword
  };
};

exports.useResetPassword = useResetPassword;