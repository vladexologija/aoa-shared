"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSaveLead = exports.WAITLIST_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const WAITLIST_MUTATION = (0, _client.gql)`
  mutation SaveLead($email: String!) {
    saveLead(email: $email)
  }
`;
exports.WAITLIST_MUTATION = WAITLIST_MUTATION;

const useSaveLead = () => {
  const [saveLead, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(WAITLIST_MUTATION);
  const saveLeadCallback = (0, _react.useCallback)(email => {
    saveLead({
      variables: {
        email
      }
    }).catch(() => null);
  }, [saveLead]);
  return {
    saveLead: saveLeadCallback,
    loading,
    error,
    data: data && data.saveLead
  };
};

exports.useSaveLead = useSaveLead;