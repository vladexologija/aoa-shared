"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSignupNewsletter = exports.SIGNUP_NEWSLETTER_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const SIGNUP_NEWSLETTER_MUTATION = (0, _client.gql)`
  mutation SignupNewsletter($email: String!) {
    signupNewsletter(input: { email: $email }) {
      success
      message
    }
  }
`;
exports.SIGNUP_NEWSLETTER_MUTATION = SIGNUP_NEWSLETTER_MUTATION;

const useSignupNewsletter = () => {
  const [signupNewsletter, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(SIGNUP_NEWSLETTER_MUTATION);
  const signupNewsletterCallback = (0, _react.useCallback)(email => {
    signupNewsletter({
      variables: {
        email
      }
    }).catch(() => null);
  }, [signupNewsletter]);
  return {
    signupNewsletter: signupNewsletterCallback,
    loading,
    error,
    success: data && data.signupNewsletter && data.signupNewsletter.success,
    message: data && data.signupNewsletter && data.signupNewsletter.message
  };
};

exports.useSignupNewsletter = useSignupNewsletter;