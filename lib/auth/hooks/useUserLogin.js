"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUserLogin = exports.USER_LOGIN_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const USER_LOGIN_MUTATION = (0, _client.gql)`
  mutation UserLogin($username: String!, $password: String!) {
    userLogin(username: $username, password: $password) {
      accessToken
      roles
    }
  }
`;
exports.USER_LOGIN_MUTATION = USER_LOGIN_MUTATION;

const useUserLogin = () => {
  const [userLogin, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(USER_LOGIN_MUTATION);
  const userLoginCallback = (0, _react.useCallback)((username, password) => {
    userLogin({
      variables: {
        username,
        password
      }
    }).catch(() => null);
  }, [userLogin]);
  return {
    userLogin: userLoginCallback,
    loading,
    error,
    data: data && data.userLogin
  };
};

exports.useUserLogin = useUserLogin;