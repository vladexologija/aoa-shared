"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCancelAutoReplenishPlan = exports.CANCEL_AUTO_REPLENISH_PLAN_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const CANCEL_AUTO_REPLENISH_PLAN_MUTATION = (0, _client.gql)`
  mutation CancelAutoReplenishPlan($id: ID!) {
    cancelAutoReplenishPlan(id: $id) {
      id
      canceledAt
      desiredDeliveryAt
    }
  }
`;
exports.CANCEL_AUTO_REPLENISH_PLAN_MUTATION = CANCEL_AUTO_REPLENISH_PLAN_MUTATION;

const useCancelAutoReplenishPlan = () => {
  const [cancelAutoReplenishPlan, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CANCEL_AUTO_REPLENISH_PLAN_MUTATION);
  const cancelAutoReplenishPlanCallback = (0, _react.useCallback)(id => {
    cancelAutoReplenishPlan({
      variables: {
        id
      }
    }).catch(() => null);
  }, [cancelAutoReplenishPlan]);
  return {
    cancelAutoReplenishPlan: cancelAutoReplenishPlanCallback,
    loading,
    error,
    data
  };
};

exports.useCancelAutoReplenishPlan = useCancelAutoReplenishPlan;