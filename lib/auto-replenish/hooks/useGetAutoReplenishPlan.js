"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetAutoReplenishPlan = exports.GET_AUTO_REPLENISH_PLAN_QUERY = void 0;

var _client = require("@apollo/client");

var _fragments = require("../../common/fragments");

const GET_AUTO_REPLENISH_PLAN_QUERY = (0, _client.gql)`
  query GetAutoReplenishPlan($id: ID!) {
    autoReplenishPlan(id: $id) {
      ...AutoReplenishPlanFields
      shippingAddress {
        ...AddressFields
      }
      paymentMethod {
        ...PaymentMethodFields
      }
    }
  }
  ${_fragments.arPlanFragment}
  ${_fragments.addressFragment}
  ${_fragments.paymentMethodFragment}
`;
exports.GET_AUTO_REPLENISH_PLAN_QUERY = GET_AUTO_REPLENISH_PLAN_QUERY;

const useGetAutoReplenishPlan = id => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_AUTO_REPLENISH_PLAN_QUERY, {
    variables: {
      id
    }
  });
  return {
    loading,
    error,
    arPlan: data && data.autoReplenishPlan
  };
};

exports.useGetAutoReplenishPlan = useGetAutoReplenishPlan;