"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetAutoReplenishPlans = exports.GET_AUTO_REPLENISH_PLANS_QUERY = void 0;

var _client = require("@apollo/client");

var _fragments = require("../../common/fragments");

const GET_AUTO_REPLENISH_PLANS_QUERY = (0, _client.gql)`
  query GetAutoReplenishPlans($customerId: ID) {
    getAutoReplenishPlans(customerId: $customerId) {
      ...AutoReplenishPlanFields
      shippingAddress {
        ...AddressFields
      }
      paymentMethod {
        ...PaymentMethodFields
      }
    }
  }
  ${_fragments.arPlanFragment}
  ${_fragments.addressFragment}
  ${_fragments.paymentMethodFragment}
`;
exports.GET_AUTO_REPLENISH_PLANS_QUERY = GET_AUTO_REPLENISH_PLANS_QUERY;

const useGetAutoReplenishPlans = customerId => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_AUTO_REPLENISH_PLANS_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: {
      customerId
    }
  });
  return {
    loading,
    error,
    arPlans: data && data.getAutoReplenishPlans || []
  };
};

exports.useGetAutoReplenishPlans = useGetAutoReplenishPlans;