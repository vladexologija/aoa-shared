"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetAutoReplenishPlansSummary = exports.GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY = void 0;

var _client = require("@apollo/client");

const GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY = (0, _client.gql)`
  query GetAutoReplenishPlansSummary {
    getAutoReplenishPlans {
      id
      desiredDeliveryAt
      product {
        id
        name
        imageUrl
      }
    }
  }
`;
exports.GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY = GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY;

const useGetAutoReplenishPlansSummary = () => {
  const {
    loading,
    error,
    data,
    refetch,
    networkStatus
  } = (0, _client.useQuery)(GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY, {
    notifyOnNetworkStatusChange: true
  });
  return {
    loading: loading || networkStatus === _client.NetworkStatus.refetch,
    error,
    arPlans: data && data.getAutoReplenishPlans || [],
    refetch
  };
};

exports.useGetAutoReplenishPlansSummary = useGetAutoReplenishPlansSummary;