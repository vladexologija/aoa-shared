"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.usePauseAutoReplenishPlan = exports.PAUSE_AUTO_REPLENISH_PLAN_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const PAUSE_AUTO_REPLENISH_PLAN_MUTATION = (0, _client.gql)`
  mutation PauseAutoReplenishPlan($id: ID!) {
    pauseAutoReplenishPlan(id: $id) {
      id
      pausedAt
      desiredDeliveryAt
    }
  }
`;
exports.PAUSE_AUTO_REPLENISH_PLAN_MUTATION = PAUSE_AUTO_REPLENISH_PLAN_MUTATION;

const usePauseAutoReplenishPlan = () => {
  const [pauseAutoReplenishPlan, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(PAUSE_AUTO_REPLENISH_PLAN_MUTATION);
  const pauseAutoReplenishPlanCallback = (0, _react.useCallback)(id => {
    pauseAutoReplenishPlan({
      variables: {
        id
      }
    }).catch(() => null);
  }, [pauseAutoReplenishPlan]);
  return {
    pauseAutoReplenishPlan: pauseAutoReplenishPlanCallback,
    loading,
    error,
    data
  };
};

exports.usePauseAutoReplenishPlan = usePauseAutoReplenishPlan;