"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useResumeAutoReplenishPlan = exports.UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION = (0, _client.gql)`
  mutation ResumeAutoReplenishPlan($id: ID!, $nextDesiredDelivery: DateTime!) {
    resumeAutoReplenishPlan(id: $id, nextDesiredDelivery: $nextDesiredDelivery) {
      id
      pausedAt
      desiredDeliveryAt
    }
  }
`;
exports.UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION = UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION;

const useResumeAutoReplenishPlan = () => {
  const [resumeAutoReplenishPlan, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION);
  const resumeAutoReplenishPlanCallback = (0, _react.useCallback)((id, nextDesiredDelivery) => {
    resumeAutoReplenishPlan({
      variables: {
        id,
        nextDesiredDelivery
      }
    }).catch(() => null);
  }, [resumeAutoReplenishPlan]);
  return {
    resumeAutoReplenishPlan: resumeAutoReplenishPlanCallback,
    loading,
    error,
    data
  };
};

exports.useResumeAutoReplenishPlan = useResumeAutoReplenishPlan;