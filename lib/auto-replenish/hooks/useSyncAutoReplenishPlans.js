"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSyncAutoReplenishPlans = exports.SYNC_AUTO_REPLENISH_PLANS_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const SYNC_AUTO_REPLENISH_PLANS_MUTATION = (0, _client.gql)`
  mutation SyncAutoReplenishPlans($ids: [ID]!, $desiredDelivery: DateTime!) {
    syncAutoReplenishPlans(ids: $ids, desiredDelivery: $desiredDelivery) {
      id
      desiredDeliveryAt
    }
  }
`;
exports.SYNC_AUTO_REPLENISH_PLANS_MUTATION = SYNC_AUTO_REPLENISH_PLANS_MUTATION;

const useSyncAutoReplenishPlans = () => {
  const [syncAutoReplenishPlans, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(SYNC_AUTO_REPLENISH_PLANS_MUTATION);
  const syncAutoReplenishPlansCallback = (0, _react.useCallback)((ids, desiredDelivery) => {
    syncAutoReplenishPlans({
      variables: {
        ids,
        desiredDelivery
      }
    }).catch(() => null);
  }, [syncAutoReplenishPlans]);
  return {
    syncAutoReplenishPlans: syncAutoReplenishPlansCallback,
    loading,
    error,
    data
  };
};

exports.useSyncAutoReplenishPlans = useSyncAutoReplenishPlans;