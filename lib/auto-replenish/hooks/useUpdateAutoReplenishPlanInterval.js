"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateAutoReplenishPlanInterval = exports.UPDATE_AUTO_REPLENISH_PLAN_INTERVAL = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const UPDATE_AUTO_REPLENISH_PLAN_INTERVAL = (0, _client.gql)`
  mutation UpdateAutoReplenishPlanInterval($id: ID!, $interval: Int!, $type: AutoReplenishIntervalType!) {
    updateAutoReplenishPlanInterval(id: $id, interval: $interval, type: $type) {
      id
      interval
      desiredDeliveryAt
    }
  }
`;
exports.UPDATE_AUTO_REPLENISH_PLAN_INTERVAL = UPDATE_AUTO_REPLENISH_PLAN_INTERVAL;

const useUpdateAutoReplenishPlanInterval = () => {
  const [updateAutoReplenishPlanInterval, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_AUTO_REPLENISH_PLAN_INTERVAL);
  const updateCallback = (0, _react.useCallback)((id, interval, type = 'MONTH') => {
    updateAutoReplenishPlanInterval({
      variables: {
        id,
        interval,
        type
      }
    }).catch(() => null);
  }, [updateAutoReplenishPlanInterval]);
  return {
    updateAutoReplenishPlanInterval: updateCallback,
    loading,
    error,
    data
  };
};

exports.useUpdateAutoReplenishPlanInterval = useUpdateAutoReplenishPlanInterval;