"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateAutoReplenishPlanNextDeliveryDate = exports.UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE = (0, _client.gql)`
  mutation UpdateAutoReplenishPlanNextDeliveryDate($id: ID!, $date: DateTime!) {
    updateAutoReplenishPlanNextDeliveryDate(id: $id, date: $date) {
      id
      desiredDeliveryAt
    }
  }
`;
exports.UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE = UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE;

const useUpdateAutoReplenishPlanNextDeliveryDate = () => {
  const [updateAutoReplenishPlanNextDeliveryDate, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE);
  const updateCallback = (0, _react.useCallback)((id, date) => {
    updateAutoReplenishPlanNextDeliveryDate({
      variables: {
        id,
        date
      }
    }).catch(() => null);
  }, [updateAutoReplenishPlanNextDeliveryDate]);
  return {
    updateAutoReplenishPlanNextDeliveryDate: updateCallback,
    loading,
    error,
    data
  };
};

exports.useUpdateAutoReplenishPlanNextDeliveryDate = useUpdateAutoReplenishPlanNextDeliveryDate;