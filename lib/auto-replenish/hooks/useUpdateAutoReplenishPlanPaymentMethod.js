"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateAutoReplenishPlanPaymentMethod = exports.UPDATE_AUTO_REPLENISH_PLAN_METHOD = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const UPDATE_AUTO_REPLENISH_PLAN_METHOD = (0, _client.gql)`
  mutation UpdateAutoReplenishPlanPaymentMethod($id: ID!, $paymentMethodId: ID!) {
    updateAutoReplenishPlanPaymentMethod(id: $id, paymentMethodId: $paymentMethodId) {
      id
      paymentMethod {
        id
        brand
        last4Digits
      }
    }
  }
`;
exports.UPDATE_AUTO_REPLENISH_PLAN_METHOD = UPDATE_AUTO_REPLENISH_PLAN_METHOD;

const useUpdateAutoReplenishPlanPaymentMethod = () => {
  const [updateAutoReplenishPlanPaymentMethod, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_AUTO_REPLENISH_PLAN_METHOD);
  const updateCallback = (0, _react.useCallback)((id, paymentMethodId) => {
    updateAutoReplenishPlanPaymentMethod({
      variables: {
        id,
        paymentMethodId
      }
    }).catch(() => null);
  }, [updateAutoReplenishPlanPaymentMethod]);
  return {
    updateAutoReplenishPlanPaymentMethod: updateCallback,
    loading,
    error,
    data
  };
};

exports.useUpdateAutoReplenishPlanPaymentMethod = useUpdateAutoReplenishPlanPaymentMethod;