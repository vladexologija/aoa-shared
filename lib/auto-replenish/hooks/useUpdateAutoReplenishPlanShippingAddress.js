"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateAutoReplenishPlanShippingAddress = exports.UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS = (0, _client.gql)`
  mutation UpdateAutoReplenishPlanShippingAddress($id: ID!, $shippingAddressId: ID!) {
    updateAutoReplenishPlanShippingAddress(id: $id, shippingAddressId: $shippingAddressId) {
      id
      shippingAddress {
        id
        line1
        line2
        city
        state
        zipCode
      }
    }
  }
`;
exports.UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS = UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS;

const useUpdateAutoReplenishPlanShippingAddress = () => {
  const [updateAutoReplenishPlanShippingAddress, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS);
  const updateCallback = (0, _react.useCallback)((id, shippingAddressId) => {
    updateAutoReplenishPlanShippingAddress({
      variables: {
        id,
        shippingAddressId
      }
    }).catch(() => null);
  }, [updateAutoReplenishPlanShippingAddress]);
  return {
    updateAutoReplenishPlanShippingAddress: updateCallback,
    loading,
    error,
    data: data && data.updateAutoReplenishPlanShippingAddress
  };
};

exports.useUpdateAutoReplenishPlanShippingAddress = useUpdateAutoReplenishPlanShippingAddress;