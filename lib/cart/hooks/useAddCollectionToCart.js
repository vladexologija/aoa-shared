"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useAddCollectionToCart = exports.ADD_COLLECTION_TO_CART = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

var _useCountCartItems = require("./useCountCartItems");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ADD_COLLECTION_TO_CART = (0, _client.gql)`
  mutation AddCollectionToCart($input: AddCollectionToCartInput!) {
    addCollectionToCart(input: $input) {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.ADD_COLLECTION_TO_CART = ADD_COLLECTION_TO_CART;

const useAddCollectionToCart = () => {
  const [addCollectionToCart, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(ADD_COLLECTION_TO_CART, {
    update(cache, {
      data: mutationResult
    }) {
      const {
        getCart
      } = cache.readQuery({
        query: _useCountCartItems.COUNT_CART_ITEMS_QUERY
      });

      if (getCart === null) {
        const cartData = (0, _get.default)(mutationResult, 'addCollectionToCart');

        if (cartData) {
          cache.writeQuery({
            query: _useCountCartItems.COUNT_CART_ITEMS_QUERY,
            data: {
              getCart: cartData
            }
          });
        }
      }
    }

  });
  const addCollectionToCartCallback = (0, _react.useCallback)(collectionId => {
    addCollectionToCart({
      variables: {
        input: {
          collectionId
        }
      }
    }).catch(() => null);
  }, [addCollectionToCart]);
  return {
    addCollectionToCart: addCollectionToCartCallback,
    loading,
    error,
    cart: (0, _get.default)(data, 'addCollectionToCart.cart'),
    warnings: (0, _get.default)(data, 'addCollectionToCart.warnings')
  };
};

exports.useAddCollectionToCart = useAddCollectionToCart;