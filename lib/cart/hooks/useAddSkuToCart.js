"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useAddSkuToCart = exports.ADD_SKU_TO_CART_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

var _useCountCartItems = require("./useCountCartItems");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ADD_SKU_TO_CART_MUTATION = (0, _client.gql)`
  mutation AddSkuToCartMutation($input: AddSkuToCartInput!) {
    addSkuToCart(input: $input) {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.ADD_SKU_TO_CART_MUTATION = ADD_SKU_TO_CART_MUTATION;

const useAddSkuToCart = () => {
  const [addSkuToCart, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(ADD_SKU_TO_CART_MUTATION, {
    update(cache, {
      data: mutationResult
    }) {
      const {
        getCart
      } = cache.readQuery({
        query: _useCountCartItems.COUNT_CART_ITEMS_QUERY
      });

      if (getCart === null) {
        const cartData = (0, _get.default)(mutationResult, 'addSkuToCart');

        if (cartData) {
          cache.writeQuery({
            query: _useCountCartItems.COUNT_CART_ITEMS_QUERY,
            data: {
              getCart: cartData
            }
          });
        }
      }
    }

  });
  const addSkuToCartCallback = (0, _react.useCallback)(({
    skuId,
    quantity = 1,
    interval = 0
  }) => {
    addSkuToCart({
      variables: {
        input: {
          skuId,
          quantity,
          autoReplenishPlan: interval > 0 ? {
            interval,
            intervalType: 'MONTH'
          } : null
        }
      }
    }).catch(() => null);
  }, [addSkuToCart]);
  return {
    addSkuToCart: addSkuToCartCallback,
    loading,
    error,
    cart: (0, _get.default)(data, 'addSkuToCart.cart'),
    warnings: (0, _get.default)(data, 'addSkuToCart.warnings')
  };
};

exports.useAddSkuToCart = useAddSkuToCart;