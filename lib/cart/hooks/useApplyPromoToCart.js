"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useApplyPromoToCart = exports.APPLY_PROMO_TO_CART_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const APPLY_PROMO_TO_CART_MUTATION = (0, _client.gql)`
  mutation ApplyPromoToCartMutation($input: ApplyPromoToCartInput!) {
    applyPromoToCart(input: $input) {
      cart {
        ...CartFields
        grandTotal
      }
      promotion {
        code
        discount
        isValid
        type
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.APPLY_PROMO_TO_CART_MUTATION = APPLY_PROMO_TO_CART_MUTATION;

const useApplyPromoToCart = () => {
  const [applyPromoToCart, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(APPLY_PROMO_TO_CART_MUTATION);
  const applyPromoToCartCallback = (0, _react.useCallback)(promoCode => {
    applyPromoToCart({
      variables: {
        input: {
          promoCode
        }
      }
    }).catch(() => null);
  }, [applyPromoToCart]);
  return {
    applyPromoToCart: applyPromoToCartCallback,
    loading,
    error,
    cart: (0, _get.default)(data, 'applyPromoToCart.cart'),
    promotion: (0, _get.default)(data, 'applyPromoToCart.promotion'),
    warnings: (0, _get.default)(data, 'applyPromoToCart.warnings')
  };
};

exports.useApplyPromoToCart = useApplyPromoToCart;