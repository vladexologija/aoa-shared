"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCheckout = exports.GET_CHECKOUT_QUERY = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _has = _interopRequireDefault(require("lodash/has"));

var _dayjs = _interopRequireDefault(require("dayjs"));

var _fragments = require("../../common/fragments");

var _utils = require("../../common/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GET_CHECKOUT_QUERY = (0, _client.gql)`
  query GetCheckout {
    getCart {
      cart {
        address {
          id
          fullName
          line1
          line2
          city
          state
          zipCode
        }
        shipping {
          service
          totalCost
          expectedArrival
        }
        taxes {
          amountToCollect
          rate
        }
        ...CartFields
        grandTotal
      }
      promotion {
        code
        discount
        isValid
        type
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.GET_CHECKOUT_QUERY = GET_CHECKOUT_QUERY;

const useCheckout = () => {
  const [getCheckout, {
    loading,
    data,
    error,
    refetch,
    networkStatus
  }] = (0, _client.useLazyQuery)(GET_CHECKOUT_QUERY, {
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange: true
  });
  const {
    cart,
    warnings,
    promotion,
    hasAddress,
    formattedSubtotals
  } = (0, _react.useMemo)(() => {
    const promotionObject = (0, _get.default)(data, 'getCart.promotion');
    const cartObject = (0, _get.default)(data, 'getCart.cart');
    const eta = (0, _get.default)(cartObject, 'shipping.expectedArrival');
    return {
      cart: cartObject,
      warnings: (0, _get.default)(data, 'getCart.warnings'),
      promotion: promotionObject && { ...promotionObject,
        discount: (0, _utils.formatPrice)(promotionObject.discount)
      },
      hasAddress: (0, _has.default)(cartObject, ['address', 'id']),
      formattedSubtotals: {
        shippingCost: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'shipping.totalCost')),
        taxes: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'taxes.amountToCollect')),
        grandTotal: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'grandTotal')),
        discountSubtotal: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'discountSubtotal'), true),
        rentalFees: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'additionalFees.rentalTotal')),
        eta: eta ? (0, _dayjs.default)(eta).format('MMMM D') : '...',
        subtotal: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'subtotal'))
      }
    };
  }, [data]);
  return {
    getCheckout,
    loading: loading || networkStatus === _client.NetworkStatus.refetch,
    error,
    cart,
    warnings,
    promotion,
    hasAddress,
    formattedSubtotals,
    refetch
  };
};

exports.useCheckout = useCheckout;