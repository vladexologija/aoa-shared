"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCountCartItems = exports.COUNT_CART_ITEMS_QUERY = void 0;

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const COUNT_CART_ITEMS_QUERY = (0, _client.gql)`
  query countCartItems {
    getCart {
      cart {
        id
        itemsCount
      }
    }
  }
`;
exports.COUNT_CART_ITEMS_QUERY = COUNT_CART_ITEMS_QUERY;

const useCountCartItems = () => {
  const {
    loading,
    data,
    error,
    refetch
  } = (0, _client.useQuery)(COUNT_CART_ITEMS_QUERY);
  return {
    refetch,
    loading,
    itemsCount: (0, _get.default)(data, 'getCart.cart.itemsCount', 0),
    error
  };
};

exports.useCountCartItems = useCountCartItems;