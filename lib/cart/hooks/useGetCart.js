"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetCart = exports.GET_CART_QUERY = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

var _utils = require("../../common/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GET_CART_QUERY = (0, _client.gql)`
  query GetCart {
    getCart {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.GET_CART_QUERY = GET_CART_QUERY;

const useGetCart = () => {
  const {
    loading,
    data,
    error,
    refetch,
    networkStatus
  } = (0, _client.useQuery)(GET_CART_QUERY, {
    notifyOnNetworkStatusChange: true
  });
  const {
    cart,
    formattedSubtotals
  } = (0, _react.useMemo)(() => {
    if (data) {
      const cartObject = (0, _get.default)(data, 'getCart.cart', null);
      const subtotals = cartObject && {
        rentalFees: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'additionalFees.rentalTotal')),
        subtotal: (0, _utils.formatPrice)((0, _get.default)(cartObject, 'subtotal'))
      };
      return {
        cart: cartObject,
        formattedSubtotals: subtotals
      };
    }

    return {
      cart: undefined,
      formattedSubtotals: undefined
    };
  }, [data]);
  return {
    loading: loading || networkStatus === _client.NetworkStatus.refetch,
    error,
    cart,
    warnings: (0, _get.default)(data, 'getCart.warnings'),
    formattedSubtotals,
    refetch
  };
};

exports.useGetCart = useGetCart;