"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRemoveItemFromCart = exports.REMOVE_ITEM_FROM_CART_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const REMOVE_ITEM_FROM_CART_MUTATION = (0, _client.gql)`
  mutation RemoveItemFromCart($input: RemoveCartItemFromCartInput!) {
    removeItemFromCart(input: $input) {
      cart {
        ...CartFields
        removedItem {
          sku {
            id
            outOfStock
            buyLimitReached
            maxItemsCanBuy
          }
        }
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.REMOVE_ITEM_FROM_CART_MUTATION = REMOVE_ITEM_FROM_CART_MUTATION;

const useRemoveItemFromCart = () => {
  const [removeItemFromCart, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(REMOVE_ITEM_FROM_CART_MUTATION);
  const removeItemFromCartCallback = (0, _react.useCallback)(id => {
    removeItemFromCart({
      variables: {
        input: {
          id
        }
      }
    }).catch(() => null);
  }, [removeItemFromCart]);
  return {
    removeItemFromCart: removeItemFromCartCallback,
    loading,
    error,
    cart: (0, _get.default)(data, 'removeItemFromCart.cart'),
    warnings: (0, _get.default)(data, 'removeItemFromCart.warnings')
  };
};

exports.useRemoveItemFromCart = useRemoveItemFromCart;