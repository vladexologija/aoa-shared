"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRemovePromoFromCart = exports.REMOVE_PROMO_FROM_CART_MUTATION = void 0;

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const REMOVE_PROMO_FROM_CART_MUTATION = (0, _client.gql)`
  mutation RemovePromoFromCartMutation {
    removePromoFromCart {
      cart {
        ...CartFields
        grandTotal
      }
      promotion {
        code
        discount
        isValid
        type
      }
    }
  }
  ${_fragments.cartFragment}
`;
exports.REMOVE_PROMO_FROM_CART_MUTATION = REMOVE_PROMO_FROM_CART_MUTATION;

const useRemovePromoFromCart = () => {
  const [removePromoFromCart, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(REMOVE_PROMO_FROM_CART_MUTATION);
  return {
    removePromoFromCart,
    loading,
    error,
    cart: (0, _get.default)(data, 'removePromoFromCart.cart'),
    warnings: (0, _get.default)(data, 'removePromoFromCart.warnings'),
    promotion: (0, _get.default)(data, 'removePromoFromCart.promotion')
  };
};

exports.useRemovePromoFromCart = useRemovePromoFromCart;