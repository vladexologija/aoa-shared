"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSetAddressToCart = exports.SET_ADDRESS_TO_CART_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const SET_ADDRESS_TO_CART_MUTATION = (0, _client.gql)`
  mutation SetAddressToCart($input: SetAddressToCartInput!) {
    setAddressToCart(input: $input) {
      cart {
        id
        subtotal
        discountSubtotal
        grandTotal
        shipping {
          service
          totalCost
          expectedArrival
        }
        taxes {
          amountToCollect
        }
        address {
          id
          fullName
          line1
          city
          state
          zipCode
        }
      }
      warnings
    }
  }
`;
exports.SET_ADDRESS_TO_CART_MUTATION = SET_ADDRESS_TO_CART_MUTATION;

const useSetAddressToCart = () => {
  const [setAddressToCart, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(SET_ADDRESS_TO_CART_MUTATION);
  const setAddressToCartCallback = (0, _react.useCallback)(addressId => {
    setAddressToCart({
      variables: {
        input: {
          id: addressId
        }
      }
    }).catch(() => null);
  }, [setAddressToCart]);
  return {
    setAddressToCart: setAddressToCartCallback,
    loading,
    error,
    cart: (0, _get.default)(data, 'setAddressToCart.cart'),
    warnings: (0, _get.default)(data, 'setAddressToCart.warnings')
  };
};

exports.useSetAddressToCart = useSetAddressToCart;