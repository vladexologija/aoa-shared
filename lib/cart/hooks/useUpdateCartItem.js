"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateCartItem = exports.UPDATE_CART_ITEM_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const UPDATE_CART_ITEM_MUTATION = (0, _client.gql)`
  mutation UpdateCartItem($input: UpdateCartItemInput!) {
    updateCartItem(input: $input) {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${_fragments.cartFragment}
`;
exports.UPDATE_CART_ITEM_MUTATION = UPDATE_CART_ITEM_MUTATION;

const useUpdateCartItem = () => {
  const [updateCartItem, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_CART_ITEM_MUTATION);
  const updateCartItemCallback = (0, _react.useCallback)(({
    itemId,
    quantity,
    interval = 0
  }) => {
    updateCartItem({
      variables: {
        input: {
          id: itemId,
          quantity,
          autoReplenishPlan: interval > 0 ? {
            interval,
            intervalType: 'MONTH'
          } : null
        }
      }
    }).catch(() => null);
  }, [updateCartItem]);
  return {
    updateCartItem: updateCartItemCallback,
    loading,
    error,
    cart: (0, _get.default)(data, 'updateCartItem.cart'),
    warnings: (0, _get.default)(data, 'updateCartItem.warnings')
  };
};

exports.useUpdateCartItem = useUpdateCartItem;