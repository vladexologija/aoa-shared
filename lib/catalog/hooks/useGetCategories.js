"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetCategories = exports.GET_CATEGORIES_QUERY = void 0;

var _client = require("@apollo/client");

const GET_CATEGORIES_QUERY = (0, _client.gql)`
  query GetCategories($withCollections: Boolean!) {
    getCategories {
      id
      name
      oneLiner
      imageUrl
      collections @include(if: $withCollections) {
        id
        name
        description
        color
        oneLiner
        invertTextColor
        products {
          id
          name
          imageUrl
          comingSoon
          productType {
            id
            name
          }
          skus {
            id
            label
            pricingInformation {
              finalPrice
            }
          }
        }
      }
    }
  }
`;
exports.GET_CATEGORIES_QUERY = GET_CATEGORIES_QUERY;

const useGetCategories = withCollections => {
  const {
    loading,
    error,
    data,
    refetch,
    networkStatus
  } = (0, _client.useQuery)(GET_CATEGORIES_QUERY, {
    variables: {
      withCollections
    },
    notifyOnNetworkStatusChange: true
  });
  return {
    loading: loading || networkStatus === _client.NetworkStatus.refetch,
    error,
    data: data && data.getCategories || [],
    refetch
  };
};

exports.useGetCategories = useGetCategories;