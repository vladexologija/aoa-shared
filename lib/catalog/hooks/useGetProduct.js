"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetProduct = exports.GET_PRODUCT_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PRODUCT_QUERY = (0, _client.gql)`
  query GetProduct($id: ID!) {
    getProduct(id: $id) {
      id
      name
      description
      seals {
        id
        imageUrl
      }
      review {
        totalReviews
        score
      }
      whatItIsTitle
      whatItIsText
      whatItIsItems {
        title
        items
      }
      whatItIsQuoteText
      whatItIsQuoteAttribution
      ingredients
      powerBehindTitle
      powerBehindSubtitle
      powerBehindItems {
        title
        text
      }
      whenToUse
      howToUse
      forBestResults
      autoReplenish
      comingSoon
      pictures {
        id
        url
        type
      }
      skus {
        id
        outOfStock
        buyLimitReached
        buyLimitDays
        buyLimit
        contentAmount
        unit
        pricingInformation {
          finalPrice
        }
      }
    }
  }
`;
exports.GET_PRODUCT_QUERY = GET_PRODUCT_QUERY;

const useGetProduct = id => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_PRODUCT_QUERY, {
    variables: {
      id
    }
  });
  return {
    loading,
    error,
    data: data && data.getProduct
  };
};

exports.useGetProduct = useGetProduct;