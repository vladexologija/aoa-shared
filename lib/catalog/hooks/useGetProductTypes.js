"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetProductTypes = exports.GET_PRODUCT_TYPES_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PRODUCT_TYPES_QUERY = (0, _client.gql)`
  query GetProductTypes {
    getProductTypes {
      id
      name
      imageUrl
    }
  }
`;
exports.GET_PRODUCT_TYPES_QUERY = GET_PRODUCT_TYPES_QUERY;

const useGetProductTypes = () => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_PRODUCT_TYPES_QUERY);
  return {
    loading,
    error,
    data: data && data.getProductTypes || []
  };
};

exports.useGetProductTypes = useGetProductTypes;