"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useNotifyWhenAvailable = exports.NOTIFY_WHEN_AVAILABLE_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const NOTIFY_WHEN_AVAILABLE_MUTATION = (0, _client.gql)`
  mutation NotifyWhenAvailable($input: NotifyWhenAvailableInput!) {
    notifyWhenAvailable(input: $input)
  }
`;
exports.NOTIFY_WHEN_AVAILABLE_MUTATION = NOTIFY_WHEN_AVAILABLE_MUTATION;

const useNotifyWhenAvailable = () => {
  const [notifyWhenAvailable, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(NOTIFY_WHEN_AVAILABLE_MUTATION);
  const notifyWhenAvailableCallback = (0, _react.useCallback)(({
    skuId,
    email
  }) => {
    notifyWhenAvailable({
      variables: {
        input: {
          skuId,
          email
        }
      }
    }).catch(() => null);
  }, [notifyWhenAvailable]);
  return {
    notifyWhenAvailable: notifyWhenAvailableCallback,
    loading,
    error,
    data: data && data.notifyWhenAvailable
  };
};

exports.useNotifyWhenAvailable = useNotifyWhenAvailable;