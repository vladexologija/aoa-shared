"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addressFragment = void 0;

var _client = require("@apollo/client");

/* istanbul ignore file */
const addressFragment = (0, _client.gql)`
  fragment AddressFields on Address {
    id
    fullName
    line1
    line2
    city
    state
    zipCode
  }
`;
exports.addressFragment = addressFragment;