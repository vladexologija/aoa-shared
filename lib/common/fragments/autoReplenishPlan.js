"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arPlanFragment = void 0;

var _client = require("@apollo/client");

/* istanbul ignore file */
const arPlanFragment = (0, _client.gql)`
  fragment AutoReplenishPlanFields on AutoReplenishPlan {
    id
    updatable
    pausedAt
    canceledAt
    initialAvailableDate
    desiredDeliveryAt
    lastShipmentAt
    lastDeliveryAt
    lastPaymentDeclinedAt
    orderHasShipped
    orderHasDelivered
    interval
    intervalType
    product {
      id
      name
      imageUrl
    }
    shipping {
      deliveryTrackingURL
    }
  }
`;
exports.arPlanFragment = arPlanFragment;