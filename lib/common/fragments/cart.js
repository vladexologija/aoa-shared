"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cartFragment = void 0;

var _client = require("@apollo/client");

/* istanbul ignore file */
const cartFragment = (0, _client.gql)`
  fragment CartFields on Cart {
    id
    itemsCount
    additionalFees {
      rentalTotal
    }
    subtotal
    discountSubtotal
    items {
      id
      quantity
      available
      autoReplenishPlan {
        interval
        intervalType
      }
      sku {
        id
        label
        outOfStock
        buyLimitReached
        buyLimitDays
        buyLimit
        maxItemsCanBuy
        pricingInformation {
          finalPrice
        }
      }
      product {
        id
        name
        imageUrl
        autoReplenish
      }
    }
  }
`;
exports.cartFragment = cartFragment;