"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.orderFragment = void 0;

var _client = require("@apollo/client");

/* istanbul ignore file */
const orderFragment = (0, _client.gql)`
  fragment OrderFields on Order {
    id
    status
    refundable
    cancelable
    canUpdatePaymentMethod
    productsTotal
    taxesTotal
    grandTotal
    rentalTotal
    createdAt
    canceledAt
    deliveredAt
    autoReplenishment
    autoReplenishPlans {
      id
    }
    items {
      id
      quantity
      price
      autoReplenishPlan {
        interval
        intervalType
      }
      sku {
        id
        product {
          id
          name
          imageUrl
        }
      }
    }
    shipping {
      deliveryTrackingCode
      deliveryTrackingURL
      estimatedDeliveryCost
    }
    shippingAddress {
      fullName
      line1
      line2
      city
      state
      zipCode
    }
    paymentMethod {
      id
      brand
      expirationMonth
      expirationYear
      last4Digits
      label
      default
    }
    refundRequest {
      reason
      evaluatedAt
      evaluationNotes
      approved
      requestedAt
      warehouseReceivedAt
      items {
        id
        quantity
        orderItem {
          sku {
            product {
              name
            }
          }
        }
      }
      productsTotal
      rentalTotal
      taxesTotal
      refundTotal
      damagedPackagesTotal
    }
  }
`;
exports.orderFragment = orderFragment;