"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.packageFragment = void 0;

var _client = require("@apollo/client");

/* istanbul ignore file */
const packageFragment = (0, _client.gql)`
  fragment PackageFields on CustomerPackage {
    id
    name
    status
    dueDate
    returnedAt
  }
`;
exports.packageFragment = packageFragment;