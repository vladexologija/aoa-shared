"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.paymentMethodFragment = void 0;

var _client = require("@apollo/client");

/* istanbul ignore file */
const paymentMethodFragment = (0, _client.gql)`
  fragment PaymentMethodFields on PaymentMethod {
    id
    brand
    expirationMonth
    expirationYear
    last4Digits
    label
  }
`;
exports.paymentMethodFragment = paymentMethodFragment;