"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _schemaTester = require("./schemaTester");

Object.keys(_schemaTester).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _schemaTester[key];
    }
  });
});