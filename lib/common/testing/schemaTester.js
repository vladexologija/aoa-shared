"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.schemaTestInvalidQuery = exports.schemaTestValidQuery = void 0;

var _easygraphqlTester = _interopRequireDefault(require("easygraphql-tester"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
const schema = _fs.default.readFileSync(_path.default.join(__dirname, '../../../', 'schema.graphql'), 'utf8');

const tester = new _easygraphqlTester.default(schema);

const schemaTestQuery = (isValid, query, params) => {
  return tester.test(isValid, query, ...params);
};

const schemaTestValidQuery = (hook, ...params) => schemaTestQuery(true, hook, params);

exports.schemaTestValidQuery = schemaTestValidQuery;

const schemaTestInvalidQuery = (hook, ...params) => schemaTestQuery(false, hook, params);

exports.schemaTestInvalidQuery = schemaTestInvalidQuery;