"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shouldDisableDateWeekend = void 0;

const shouldDisableDateWeekend = currentDate => currentDate.day() === 0;

exports.shouldDisableDateWeekend = shouldDisableDateWeekend;