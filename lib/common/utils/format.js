"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatCardNumber = exports.formatAutoReplenishPlan = exports.formatDate = exports.DATE_FORMAT = exports.formatCardExpirationDate = exports.formatPrice = void 0;

var _capitalize = _interopRequireDefault(require("lodash/capitalize"));

var _dayjs = _interopRequireDefault(require("dayjs"));

var _accounting = _interopRequireDefault(require("accounting"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const formatPrice = (priceInteger, skipDefaultValue = false) => {
  if (priceInteger || priceInteger === 0) {
    return _accounting.default.formatMoney(priceInteger / 100);
  }

  return skipDefaultValue ? priceInteger : '---';
};

exports.formatPrice = formatPrice;

const formatCardExpirationDate = (expirationYear, expirationMonth) => {
  return `${expirationMonth.toString().padStart(2, '0')}/${expirationYear}`;
};

exports.formatCardExpirationDate = formatCardExpirationDate;
const DATE_FORMAT = 'MM/DD/YYYY';
exports.DATE_FORMAT = DATE_FORMAT;

const formatDate = dateString => {
  return (0, _dayjs.default)(dateString).format(DATE_FORMAT);
};

exports.formatDate = formatDate;

const formatAutoReplenishPlan = arInfo => {
  let output = 'Every ';

  if (arInfo.interval === 1) {
    output += (0, _capitalize.default)(arInfo.intervalType);
  } else {
    output += `${arInfo.interval} ${(0, _capitalize.default)(`${arInfo.intervalType}s`)}`;
  }

  return output;
};

exports.formatAutoReplenishPlan = formatAutoReplenishPlan;

const formatCardNumber = (last4, prePadFull) => {
  let prePadding = '**** ';

  if (prePadFull) {
    prePadding += '**** **** ';
  }

  return `${prePadding}${last4}`;
};

exports.formatCardNumber = formatCardNumber;