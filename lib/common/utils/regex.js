"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PHONE_REGEX = exports.ZIPCODE_REGEX = exports.STATE_REGEX = exports.EMAIL_REGEX = void 0;
// eslint-disable-next-line no-control-regex
const EMAIL_REGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
exports.EMAIL_REGEX = EMAIL_REGEX;
const STATE_REGEX = /^(?:(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]))$/;
exports.STATE_REGEX = STATE_REGEX;
const ZIPCODE_REGEX = /^\d{5}(?:[-\s]\d{4})?$/;
exports.ZIPCODE_REGEX = ZIPCODE_REGEX;
const PHONE_REGEX = /^[0-9 \-()]{10,}$/;
exports.PHONE_REGEX = PHONE_REGEX;