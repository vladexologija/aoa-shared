"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isValidPhoneNumber = void 0;

var _regex = require("./regex");

const isValidPhoneNumber = value => {
  const numberOfDigits = value.match(/\d/g).length;
  return numberOfDigits === 10 && _regex.PHONE_REGEX.test(value);
};

exports.isValidPhoneNumber = isValidPhoneNumber;