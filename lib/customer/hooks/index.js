"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useGetProfile = require("./useGetProfile");

Object.keys(_useGetProfile).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useGetProfile[key];
    }
  });
});

var _useUpdateCustomer = require("./useUpdateCustomer");

Object.keys(_useUpdateCustomer).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useUpdateCustomer[key];
    }
  });
});