"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetProfile = exports.GET_PROFILE_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PROFILE_QUERY = (0, _client.gql)`
  query GetProfile {
    profile {
      email
      firstName
      lastName
    }
  }
`;
exports.GET_PROFILE_QUERY = GET_PROFILE_QUERY;

const useGetProfile = () => {
  const {
    data,
    error,
    loading,
    refetch,
    networkStatus
  } = (0, _client.useQuery)(GET_PROFILE_QUERY, {
    notifyOnNetworkStatusChange: true
  });
  return {
    error,
    loading: loading || networkStatus === _client.NetworkStatus.refetch,
    data: data && data.profile,
    refetch
  };
};

exports.useGetProfile = useGetProfile;