"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateCustomer = exports.UPDATE_CUSTOMER_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const UPDATE_CUSTOMER_MUTATION = (0, _client.gql)`
  mutation UpdateCustomer($input: UpdateCustomerInput!) {
    updateCustomer(input: $input) {
      id
    }
  }
`;
exports.UPDATE_CUSTOMER_MUTATION = UPDATE_CUSTOMER_MUTATION;

const useUpdateCustomer = () => {
  const [updateCustomer, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_CUSTOMER_MUTATION);
  const updateCustomerCallback = (0, _react.useCallback)(({
    oldPassword,
    newPassword
  }) => {
    updateCustomer({
      variables: {
        input: {
          oldPassword,
          newPassword
        }
      }
    }).catch(() => null);
  }, [updateCustomer]);
  return {
    updateCustomer: updateCustomerCallback,
    loading,
    error,
    data: data && data.updateCustomer
  };
};

exports.useUpdateCustomer = useUpdateCustomer;