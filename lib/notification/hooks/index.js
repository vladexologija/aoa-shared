"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useCountUnreadNotifications = require("./useCountUnreadNotifications");

Object.keys(_useCountUnreadNotifications).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useCountUnreadNotifications[key];
    }
  });
});

var _useGetNotifications = require("./useGetNotifications");

Object.keys(_useGetNotifications).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useGetNotifications[key];
    }
  });
});

var _useMarkNotificationRead = require("./useMarkNotificationRead");

Object.keys(_useMarkNotificationRead).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useMarkNotificationRead[key];
    }
  });
});