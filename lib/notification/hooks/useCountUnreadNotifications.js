"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCountUnreadNotifications = exports.COUNT_NOTIFICATIONS_QUERY = void 0;

var _client = require("@apollo/client");

const COUNT_NOTIFICATIONS_QUERY = (0, _client.gql)`
  query CountUnreadNotifications {
    countUnreadNotifications
  }
`;
exports.COUNT_NOTIFICATIONS_QUERY = COUNT_NOTIFICATIONS_QUERY;

const useCountUnreadNotifications = () => {
  const [countNotifications, {
    loading,
    data,
    error
  }] = (0, _client.useLazyQuery)(COUNT_NOTIFICATIONS_QUERY, {
    fetchPolicy: 'cache-and-network'
  });
  return {
    loading,
    error,
    data: data && data.countUnreadNotifications,
    countNotifications
  };
};

exports.useCountUnreadNotifications = useCountUnreadNotifications;