"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetNotifications = exports.GET_NOTIFICATIONS = void 0;

var _client = require("@apollo/client");

const GET_NOTIFICATIONS = (0, _client.gql)`
  query GetNotifications {
    getNotifications {
      id
      message
      readAt
    }
  }
`;
exports.GET_NOTIFICATIONS = GET_NOTIFICATIONS;

const useGetNotifications = () => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_NOTIFICATIONS, {
    fetchPolicy: 'cache-and-network'
  });
  return {
    loading,
    error,
    data: data ? data.getNotifications : []
  };
};

exports.useGetNotifications = useGetNotifications;