"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useMarkNotificationRead = exports.MARK_READ = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const MARK_READ = (0, _client.gql)`
  mutation MarkRead($ids: [ID!]!) {
    markNotificationRead(ids: $ids)
  }
`;
exports.MARK_READ = MARK_READ;

const useMarkNotificationRead = () => {
  const [markRead, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(MARK_READ);
  const markReadCallback = (0, _react.useCallback)(ids => {
    markRead({
      variables: {
        ids
      }
    }).catch(() => null);
  }, [markRead]);
  return {
    markRead: markReadCallback,
    loading,
    error,
    data: data && data.markRead
  };
};

exports.useMarkNotificationRead = useMarkNotificationRead;