"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCancelOrder = exports.CANCEL_ORDER_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const CANCEL_ORDER_MUTATION = (0, _client.gql)`
  mutation CancelOrder($id: ID!, $reason: String) {
    cancelOrder(id: $id, reason: $reason) {
      id
      refundable
      cancelable
      canUpdatePaymentMethod
      canceledAt
      status
    }
  }
`;
exports.CANCEL_ORDER_MUTATION = CANCEL_ORDER_MUTATION;

const useCancelOrder = () => {
  const [cancelOrder, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CANCEL_ORDER_MUTATION);
  const cancelOrderCallback = (0, _react.useCallback)((id, reason) => {
    return cancelOrder({
      variables: {
        id,
        reason
      }
    });
  }, [cancelOrder]);
  return {
    cancelOrder: cancelOrderCallback,
    loading,
    error,
    order: data && data.cancelOrder
  };
};

exports.useCancelOrder = useCancelOrder;