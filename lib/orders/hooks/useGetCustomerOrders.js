"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetCustomerOrders = exports.GET_ORDERS_QUERY = void 0;

var _client = require("@apollo/client");

var _fragments = require("../../common/fragments");

const GET_ORDERS_QUERY = (0, _client.gql)`
  query GetCustomerOrders {
    getCustomerOrders {
      ...OrderFields
    }
  }
  ${_fragments.orderFragment}
`;
exports.GET_ORDERS_QUERY = GET_ORDERS_QUERY;

const useGetCustomerOrders = () => {
  const {
    loading,
    data,
    error
  } = (0, _client.useQuery)(GET_ORDERS_QUERY, {
    fetchPolicy: 'cache-and-network'
  });
  return {
    loading,
    error,
    orders: data && data.getCustomerOrders || []
  };
};

exports.useGetCustomerOrders = useGetCustomerOrders;