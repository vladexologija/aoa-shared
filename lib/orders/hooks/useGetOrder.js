"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetOrder = exports.GET_ORDER_QUERY = void 0;

var _client = require("@apollo/client");

var _fragments = require("../../common/fragments");

const GET_ORDER_QUERY = (0, _client.gql)`
  query GetOrder($id: ID!) {
    getOrder(id: $id) {
      ...OrderFields
    }
  }
  ${_fragments.orderFragment}
`;
exports.GET_ORDER_QUERY = GET_ORDER_QUERY;

const useGetOrder = id => {
  const {
    loading,
    data,
    error
  } = (0, _client.useQuery)(GET_ORDER_QUERY, {
    variables: {
      id
    }
  });
  return {
    loading,
    error,
    order: data && data.getOrder
  };
};

exports.useGetOrder = useGetOrder;