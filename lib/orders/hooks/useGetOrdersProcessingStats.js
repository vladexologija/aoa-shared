"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetOrdersProcessingStats = exports.GET_ORDERS_PROCESSING_STATS = void 0;

var _client = require("@apollo/client");

const GET_ORDERS_PROCESSING_STATS = (0, _client.gql)`
  query GetOrdersProcessingStats {
    getOrdersProcessingStats {
      waitingForProcessing
      waitingForStock
    }
  }
`;
exports.GET_ORDERS_PROCESSING_STATS = GET_ORDERS_PROCESSING_STATS;

const useGetOrdersProcessingStats = () => {
  const [getOrderStats, {
    loading,
    data,
    error
  }] = (0, _client.useLazyQuery)(GET_ORDERS_PROCESSING_STATS, {
    fetchPolicy: 'cache-and-network'
  });
  return {
    getOrderStats,
    loading,
    data: data && data.getOrdersProcessingStats,
    error
  };
};

exports.useGetOrdersProcessingStats = useGetOrdersProcessingStats;