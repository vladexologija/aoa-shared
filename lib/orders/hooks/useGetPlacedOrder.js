"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPlacedOrder = exports.GET_PLACED_ORDER_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PLACED_ORDER_QUERY = (0, _client.gql)`
  query GetPlacedOrder($id: ID!) {
    getOrder(id: $id) {
      id
    }
  }
`;
exports.GET_PLACED_ORDER_QUERY = GET_PLACED_ORDER_QUERY;

const useGetPlacedOrder = id => {
  const {
    loading,
    data,
    error
  } = (0, _client.useQuery)(GET_PLACED_ORDER_QUERY, {
    variables: {
      id
    }
  });
  return {
    loading,
    error,
    order: data && data.getOrder
  };
};

exports.useGetPlacedOrder = useGetPlacedOrder;