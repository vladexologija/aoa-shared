"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetRefunds = exports.GET_REFUNDS_QUERY = void 0;

var _client = require("@apollo/client");

const GET_REFUNDS_QUERY = (0, _client.gql)`
  query GetRefunds($filters: RefundFiltersInput) {
    getRefunds(filters: $filters) {
      id
      evaluatedAt
      evaluationNotes
      approved
      warehouseReceivedAt
      requestedAt
      items {
        orderItem {
          id
          sku {
            product {
              name
            }
          }
        }
        quantity
        receivedQuantity
      }
    }
  }
`;
exports.GET_REFUNDS_QUERY = GET_REFUNDS_QUERY;

const useGetRefunds = filters => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_REFUNDS_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: {
      filters
    }
  });
  return {
    loading,
    error,
    data: data && data.getRefunds || []
  };
};

exports.useGetRefunds = useGetRefunds;