"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.usePlaceOrder = exports.PLACE_ORDER_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _hooks = require("../../cart/hooks");

var _useGetCustomerOrders = require("./useGetCustomerOrders");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const PLACE_ORDER_MUTATION = (0, _client.gql)`
  mutation PlaceOrder($input: PlaceOrderInput!) {
    placeOrder(input: $input) {
      id
    }
  }
`;
exports.PLACE_ORDER_MUTATION = PLACE_ORDER_MUTATION;

const usePlaceOrder = () => {
  const [placeOrder, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(PLACE_ORDER_MUTATION, {
    refetchQueries: [{
      query: _useGetCustomerOrders.GET_ORDERS_QUERY
    }],

    update(cache, {
      data: mutationResult
    }) {
      if ((0, _get.default)(mutationResult, 'placeOrder.id')) {
        cache.writeQuery({
          query: _hooks.GET_CART_QUERY,
          data: {
            getCart: null
          }
        });
        cache.gc();
      }
    }

  });
  const placeOrderCallback = (0, _react.useCallback)(({
    paymentMethodId,
    primariesToReturnIds,
    billingAddressId
  }) => {
    placeOrder({
      variables: {
        input: {
          paymentMethodId,
          primariesToReturnIds,
          billingAddressId
        }
      }
    }).catch(() => null);
  }, [placeOrder]);
  return {
    placeOrder: placeOrderCallback,
    loading,
    error,
    order: (0, _get.default)(data, 'placeOrder')
  };
};

exports.usePlaceOrder = usePlaceOrder;