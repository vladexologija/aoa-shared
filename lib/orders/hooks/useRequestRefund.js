"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRequestRefund = exports.REQUEST_REFUND_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const REQUEST_REFUND_MUTATION = (0, _client.gql)`
  mutation RequestRefund($orderId: ID!, $items: [RefundItemInput]!, $reason: String!) {
    requestRefund(orderId: $orderId, items: $items, reason: $reason) {
      id
      order {
        id
        refundable
        status
      }
    }
  }
`;
exports.REQUEST_REFUND_MUTATION = REQUEST_REFUND_MUTATION;

const useRequestRefund = () => {
  const [requestRefund, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(REQUEST_REFUND_MUTATION);
  const requestRefundCallback = (0, _react.useCallback)((orderId, items, reason) => {
    requestRefund({
      variables: {
        orderId,
        items,
        reason
      }
    }).catch(() => null);
  }, [requestRefund]);
  return {
    requestRefund: requestRefundCallback,
    loading,
    error,
    data: data && data.requestRefund
  };
};

exports.useRequestRefund = useRequestRefund;