"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSimulateRefund = exports.SIMULATE_REFUND_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const SIMULATE_REFUND_MUTATION = (0, _client.gql)`
  mutation simulateRefund($orderId: ID!, $items: [RefundItemInput]!) {
    simulateRefund(orderId: $orderId, items: $items) {
      items {
        product {
          name
        }
        id
        quantity
        price
      }
      rentalTotal
      taxesTotal
      refundTotal
    }
  }
`;
exports.SIMULATE_REFUND_MUTATION = SIMULATE_REFUND_MUTATION;

const useSimulateRefund = () => {
  const [simulateRefund, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(SIMULATE_REFUND_MUTATION);
  const simulateRefundCallback = (0, _react.useCallback)((orderId, items) => {
    simulateRefund({
      variables: {
        orderId,
        items
      }
    }).catch(() => null);
  }, [simulateRefund]);
  return {
    simulateRefund: simulateRefundCallback,
    loading,
    error,
    data: data && data.simulateRefund
  };
};

exports.useSimulateRefund = useSimulateRefund;