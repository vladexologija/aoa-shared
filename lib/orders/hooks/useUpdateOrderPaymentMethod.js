"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateOrderPaymentMethod = exports.UPDATE_ORDER_PAYMENT_METHOD = void 0;

var _client = require("@apollo/client");

var _react = require("react");

const UPDATE_ORDER_PAYMENT_METHOD = (0, _client.gql)`
  mutation UpdateOrderPaymentMethod($input: UpdateOrderPaymentInput!) {
    updateOrderPaymentMethod(input: $input) {
      id
      paymentCaptureFailedAt
      status
      canUpdatePaymentMethod
      paymentMethod {
        id
        brand
        expirationMonth
        expirationYear
        last4Digits
        label
        default
      }
    }
  }
`;
exports.UPDATE_ORDER_PAYMENT_METHOD = UPDATE_ORDER_PAYMENT_METHOD;

const useUpdateOrderPaymentMethod = () => {
  const [updateOrderPaymentMethodMutation, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(UPDATE_ORDER_PAYMENT_METHOD);
  const updateOrderPaymentMethod = (0, _react.useCallback)((orderId, paymentMethodId) => updateOrderPaymentMethodMutation({
    variables: {
      input: {
        orderId,
        paymentMethodId
      }
    }
  }).catch(() => null), [updateOrderPaymentMethodMutation]);
  return {
    error,
    loading,
    updateOrderPaymentMethod,
    order: data && data.updateOrderPaymentMethod
  };
};

exports.useUpdateOrderPaymentMethod = useUpdateOrderPaymentMethod;