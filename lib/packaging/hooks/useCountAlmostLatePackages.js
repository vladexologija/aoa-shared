"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCountAlmostLatePackages = exports.COUNT_ALMOST_LATE_PACKAGES_QUERY = void 0;

var _client = require("@apollo/client");

const COUNT_ALMOST_LATE_PACKAGES_QUERY = (0, _client.gql)`
  query CountAlmostLatePackages {
    countPackagesCloseToDueDate
  }
`;
exports.COUNT_ALMOST_LATE_PACKAGES_QUERY = COUNT_ALMOST_LATE_PACKAGES_QUERY;

const useCountAlmostLatePackages = () => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(COUNT_ALMOST_LATE_PACKAGES_QUERY);
  return {
    loading,
    error,
    closeToDueDateCount: data && data.countPackagesCloseToDueDate || 0
  };
};

exports.useCountAlmostLatePackages = useCountAlmostLatePackages;