"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetMyPackaging = exports.GET_PACKAGING_QUERY = void 0;

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

var _fragments = require("../../common/fragments");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GET_PACKAGING_QUERY = (0, _client.gql)`
  query GetMyPackaging($customerId: ID, $inUse: Boolean) {
    getMyPackaging(inUse: $inUse, customerId: $customerId) {
      shippers {
        ...PackageFields
      }
      primaries {
        ...PackageFields
        product {
          name
          imageUrl
        }
      }
    }
  }
  ${_fragments.packageFragment}
`;
exports.GET_PACKAGING_QUERY = GET_PACKAGING_QUERY;

const useGetMyPackaging = ({
  inUse,
  customerId
}) => {
  const {
    loading,
    error,
    data,
    refetch
  } = (0, _client.useQuery)(GET_PACKAGING_QUERY, {
    variables: {
      inUse,
      customerId
    }
  });
  return {
    refetch,
    loading,
    error,
    primaries: (0, _get.default)(data, 'getMyPackaging.primaries', []),
    shippers: (0, _get.default)(data, 'getMyPackaging.shippers', [])
  };
};

exports.useGetMyPackaging = useGetMyPackaging;