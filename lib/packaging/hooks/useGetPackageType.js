"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPackageType = exports.GET_PACKAGE_TYPE_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PACKAGE_TYPE_QUERY = (0, _client.gql)`
  query GetPackageType($id: ID!) {
    getPackageType(id: $id) {
      id
      name
    }
  }
`;
exports.GET_PACKAGE_TYPE_QUERY = GET_PACKAGE_TYPE_QUERY;

const useGetPackageType = id => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_PACKAGE_TYPE_QUERY, {
    variables: {
      id
    }
  });
  return {
    loading,
    error,
    data: data && data.getPackageType
  };
};

exports.useGetPackageType = useGetPackageType;