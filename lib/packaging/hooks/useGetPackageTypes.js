"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPackageTypes = exports.GET_PACKAGE_TYPES_QUERY = void 0;

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GET_PACKAGE_TYPES_QUERY = (0, _client.gql)`
  query GetPackageTypes {
    getPackageTypes {
      shippers {
        id
        name
        label
      }
      primaries {
        id
        name
        label
      }
    }
  }
`;
exports.GET_PACKAGE_TYPES_QUERY = GET_PACKAGE_TYPES_QUERY;

const useGetPackageTypes = () => {
  const {
    data,
    error,
    loading
  } = (0, _client.useQuery)(GET_PACKAGE_TYPES_QUERY);
  return {
    error,
    loading,
    shippers: (0, _get.default)(data, 'getPackageTypes.shippers', []),
    primaries: (0, _get.default)(data, 'getPackageTypes.primaries', [])
  };
};

exports.useGetPackageTypes = useGetPackageTypes;