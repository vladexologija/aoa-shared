"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPackagingUsed = exports.GET_PACKAGING_USED_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PACKAGING_USED_QUERY = (0, _client.gql)`
  query GetPackagingUsed {
    packagingUsed {
      id
      returnedAt
      status
      name
      packageType {
        name
        isShipper
      }
      product {
        id
        name
        imageUrl
      }
    }
  }
`;
exports.GET_PACKAGING_USED_QUERY = GET_PACKAGING_USED_QUERY;

const useGetPackagingUsed = () => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_PACKAGING_USED_QUERY);
  return {
    loading,
    error,
    packaging: data && data.packagingUsed || []
  };
};

exports.useGetPackagingUsed = useGetPackagingUsed;