"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPrimariesInUse = exports.GET_PRIMARIES_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PRIMARIES_QUERY = (0, _client.gql)`
  query GetPrimaries {
    primariesInUse {
      id
      dueDate
      status
      product {
        id
        name
        imageUrl
      }
    }
  }
`;
exports.GET_PRIMARIES_QUERY = GET_PRIMARIES_QUERY;

const useGetPrimariesInUse = () => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_PRIMARIES_QUERY);
  return {
    loading,
    error,
    primaries: data && data.primariesInUse || []
  };
};

exports.useGetPrimariesInUse = useGetPrimariesInUse;