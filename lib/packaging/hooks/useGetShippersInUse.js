"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetShippersInUse = exports.GET_SHIPPERS_QUERY = void 0;

var _client = require("@apollo/client");

const GET_SHIPPERS_QUERY = (0, _client.gql)`
  query GetShippers {
    shippersInUse {
      id
      dueDate
      status
      name
    }
  }
`;
exports.GET_SHIPPERS_QUERY = GET_SHIPPERS_QUERY;

const useGetShippersInUse = () => {
  const {
    loading,
    error,
    data
  } = (0, _client.useQuery)(GET_SHIPPERS_QUERY);
  return {
    loading,
    error,
    shippers: data && data.shippersInUse || []
  };
};

exports.useGetShippersInUse = useGetShippersInUse;