"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useReprintReturnLabel = exports.REPRINT_RETURN_LABEL_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const REPRINT_RETURN_LABEL_MUTATION = (0, _client.gql)`
  mutation reprintReturnLabel($id: ID!) {
    reprintReturnLabel(customerPackageId: $id)
  }
`;
exports.REPRINT_RETURN_LABEL_MUTATION = REPRINT_RETURN_LABEL_MUTATION;

const useReprintReturnLabel = () => {
  const [reprintReturnLabelMutation, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(REPRINT_RETURN_LABEL_MUTATION);
  const reprintReturnLabel = (0, _react.useCallback)(id => {
    reprintReturnLabelMutation({
      variables: {
        id
      }
    }).catch(() => null);
  }, [reprintReturnLabelMutation]);
  return {
    reprintReturnLabel,
    loading,
    error,
    returnLabelUrl: data
  };
};

exports.useReprintReturnLabel = useReprintReturnLabel;