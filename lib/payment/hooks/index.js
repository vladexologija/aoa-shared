"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useCreatePaymentSetupIntent = require("./useCreatePaymentSetupIntent");

Object.keys(_useCreatePaymentSetupIntent).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useCreatePaymentSetupIntent[key];
    }
  });
});

var _useDeletePaymentMethod = require("./useDeletePaymentMethod");

Object.keys(_useDeletePaymentMethod).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useDeletePaymentMethod[key];
    }
  });
});

var _useGetPaymentMethods = require("./useGetPaymentMethods");

Object.keys(_useGetPaymentMethods).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useGetPaymentMethods[key];
    }
  });
});

var _useSavePaymentMethod = require("./useSavePaymentMethod");

Object.keys(_useSavePaymentMethod).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useSavePaymentMethod[key];
    }
  });
});