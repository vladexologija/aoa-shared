"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCreatePaymentSetupIntent = exports.CREATE_PAYMENT_SETUP_INTENT_MUTATION = void 0;

var _client = require("@apollo/client");

const CREATE_PAYMENT_SETUP_INTENT_MUTATION = (0, _client.gql)`
  mutation createPaymentSetupIntent {
    createPaymentSetupIntent {
      id
      clientSecret
    }
  }
`;
exports.CREATE_PAYMENT_SETUP_INTENT_MUTATION = CREATE_PAYMENT_SETUP_INTENT_MUTATION;

const useCreatePaymentSetupIntent = () => {
  const [createPaymentSetupIntent, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CREATE_PAYMENT_SETUP_INTENT_MUTATION);
  return {
    createPaymentSetupIntent,
    loading,
    error,
    data: data && data.createPaymentSetupIntent
  };
};

exports.useCreatePaymentSetupIntent = useCreatePaymentSetupIntent;