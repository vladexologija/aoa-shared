"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useDeletePaymentMethod = exports.DELETE_PAYMENT_METHOD_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _useGetPaymentMethods = require("./useGetPaymentMethods");

const DELETE_PAYMENT_METHOD_MUTATION = (0, _client.gql)`
  mutation deletePaymentMethod($id: ID!) {
    deletePaymentMethod(id: $id)
  }
`;
exports.DELETE_PAYMENT_METHOD_MUTATION = DELETE_PAYMENT_METHOD_MUTATION;

const useDeletePaymentMethod = () => {
  const [deletePaymentMethod, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(DELETE_PAYMENT_METHOD_MUTATION, {
    refetchQueries: [{
      query: _useGetPaymentMethods.GET_PAYMENT_METHODS_QUERY
    }]
  });
  const deletePaymentMethodCallback = (0, _react.useCallback)(id => {
    deletePaymentMethod({
      variables: {
        id
      }
    }).catch(() => null);
  }, [deletePaymentMethod]);
  return {
    deletePaymentMethod: deletePaymentMethodCallback,
    loading,
    error,
    data
  };
};

exports.useDeletePaymentMethod = useDeletePaymentMethod;