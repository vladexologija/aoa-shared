"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPaymentMethods = exports.GET_PAYMENT_METHODS_QUERY = void 0;

var _client = require("@apollo/client");

var _fragments = require("../../common/fragments");

const GET_PAYMENT_METHODS_QUERY = (0, _client.gql)`
  query GetPaymentMethodsQuery($customerId: ID) {
    getPaymentMethods(customerId: $customerId) {
      ...PaymentMethodFields
      default
      hasAutoReplenishPlans
    }
  }
  ${_fragments.paymentMethodFragment}
`;
exports.GET_PAYMENT_METHODS_QUERY = GET_PAYMENT_METHODS_QUERY;

const useGetPaymentMethods = customerId => {
  const {
    loading,
    data,
    error,
    networkStatus,
    refetch
  } = (0, _client.useQuery)(GET_PAYMENT_METHODS_QUERY, {
    notifyOnNetworkStatusChange: true,
    variables: {
      customerId
    }
  });
  return {
    loading: loading || networkStatus === _client.NetworkStatus.refetch,
    error,
    paymentMethods: data && data.getPaymentMethods || [],
    refetch
  };
};

exports.useGetPaymentMethods = useGetPaymentMethods;