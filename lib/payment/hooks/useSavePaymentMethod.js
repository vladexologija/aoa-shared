"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSavePaymentMethod = exports.SAVE_PAYMENT_METHOD_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _fragments = require("../../common/fragments");

var _useGetPaymentMethods = require("./useGetPaymentMethods");

const SAVE_PAYMENT_METHOD_MUTATION = (0, _client.gql)`
  mutation SavePaymentMethod($paymentMethod: PaymentMethodInput!) {
    savePaymentMethod(paymentMethod: $paymentMethod) {
      ...PaymentMethodFields
      default
      hasAutoReplenishPlans
    }
  }
  ${_fragments.paymentMethodFragment}
`;
exports.SAVE_PAYMENT_METHOD_MUTATION = SAVE_PAYMENT_METHOD_MUTATION;

const useSavePaymentMethod = () => {
  const [savePaymentMethod, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(SAVE_PAYMENT_METHOD_MUTATION, {
    refetchQueries: [{
      query: _useGetPaymentMethods.GET_PAYMENT_METHODS_QUERY
    }]
  });
  const savePaymentMethodCallback = (0, _react.useCallback)(paymentMethod => {
    savePaymentMethod({
      variables: {
        paymentMethod
      }
    }).catch(() => null);
  }, [savePaymentMethod]);
  return {
    savePaymentMethod: savePaymentMethodCallback,
    loading,
    error,
    data
  };
};

exports.useSavePaymentMethod = useSavePaymentMethod;