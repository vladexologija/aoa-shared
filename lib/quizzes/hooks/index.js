"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useAnswerQuiz = require("./useAnswerQuiz");

Object.keys(_useAnswerQuiz).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useAnswerQuiz[key];
    }
  });
});

var _useGetCurrentQuiz = require("./useGetCurrentQuiz");

Object.keys(_useGetCurrentQuiz).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useGetCurrentQuiz[key];
    }
  });
});