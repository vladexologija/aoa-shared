"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useAnswerQuiz = exports.ANSWER_QUIZ_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

const ANSWER_QUIZ_MUTATION = (0, _client.gql)`
  mutation AnswerQuiz($input: AnswerQuizInput!) {
    answerQuiz(input: $input) {
      id
      planetScore
      balance
    }
  }
`;
exports.ANSWER_QUIZ_MUTATION = ANSWER_QUIZ_MUTATION;

const useAnswerQuiz = () => {
  const [answerQuiz, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(ANSWER_QUIZ_MUTATION);
  const answerQuizCallback = (0, _react.useCallback)(({
    id,
    responses
  }) => {
    answerQuiz({
      variables: {
        input: {
          id,
          responses
        }
      }
    }).catch(() => null);
  }, [answerQuiz]);
  return {
    answerQuiz: answerQuizCallback,
    loading,
    error,
    data: data && data.answerQuiz
  };
};

exports.useAnswerQuiz = useAnswerQuiz;