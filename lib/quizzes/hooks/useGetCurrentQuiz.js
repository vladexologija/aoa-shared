"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetCurrentQuiz = exports.GET_CURRENT_QUIZ_QUERY = void 0;

var _client = require("@apollo/client");

const GET_CURRENT_QUIZ_QUERY = (0, _client.gql)`
  query GetCurrentQuiz {
    currentQuiz {
      id
      name
      description
      questions {
        id
        content
        answer
      }
    }
  }
`;
exports.GET_CURRENT_QUIZ_QUERY = GET_CURRENT_QUIZ_QUERY;

const useGetCurrentQuiz = () => {
  const {
    data,
    error,
    loading
  } = (0, _client.useQuery)(GET_CURRENT_QUIZ_QUERY);
  return {
    error,
    loading,
    currentQuiz: data && data.currentQuiz
  };
};

exports.useGetCurrentQuiz = useGetCurrentQuiz;