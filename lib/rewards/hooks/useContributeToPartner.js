"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useContributeToPartner = exports.CONTRIBUTE_TO_PARTNER_MUTATION = void 0;

var _react = require("react");

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CONTRIBUTE_TO_PARTNER_MUTATION = (0, _client.gql)`
  mutation ContributeToPartner($input: ContributeInput!) {
    contributeToPartner(input: $input) {
      transferSucceed
      customerBalance {
        id
        balance
      }
      partner {
        id
        allTimeDonations
        thisYearDonations
      }
    }
  }
`;
exports.CONTRIBUTE_TO_PARTNER_MUTATION = CONTRIBUTE_TO_PARTNER_MUTATION;

const useContributeToPartner = () => {
  const [contributeToPartner, {
    data,
    loading,
    error
  }] = (0, _client.useMutation)(CONTRIBUTE_TO_PARTNER_MUTATION);
  const contributeToPartnerCallback = (0, _react.useCallback)((id, amount) => {
    contributeToPartner({
      variables: {
        input: {
          id,
          amount
        }
      }
    }).catch(() => null);
  }, [contributeToPartner]);
  return {
    contributeToPartner: contributeToPartnerCallback,
    loading,
    error,
    transferSucceed: (0, _get.default)(data, 'contributeToPartner.transferSucceed')
  };
};

exports.useContributeToPartner = useContributeToPartner;