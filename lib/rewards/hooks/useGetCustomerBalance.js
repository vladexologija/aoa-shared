"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetCustomerBalance = exports.GET_CUSTOMER_BALANCE_QUERY = void 0;

var _client = require("@apollo/client");

var _get = _interopRequireDefault(require("lodash/get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GET_CUSTOMER_BALANCE_QUERY = (0, _client.gql)`
  query GetCustomerBalance {
    getCustomerBalance {
      id
      balance
      planetScore
    }
  }
`;
exports.GET_CUSTOMER_BALANCE_QUERY = GET_CUSTOMER_BALANCE_QUERY;

const useGetCustomerBalance = () => {
  const {
    data,
    error,
    loading
  } = (0, _client.useQuery)(GET_CUSTOMER_BALANCE_QUERY);
  return {
    error,
    loading,
    balance: (0, _get.default)(data, 'getCustomerBalance.balance', 0),
    planetScore: (0, _get.default)(data, 'getCustomerBalance.planetScore', '')
  };
};

exports.useGetCustomerBalance = useGetCustomerBalance;