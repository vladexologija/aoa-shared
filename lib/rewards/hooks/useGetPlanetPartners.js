"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPlanetPartners = exports.GET_PARTNERS_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PARTNERS_QUERY = (0, _client.gql)`
  query GetPlanetPartners {
    getCharities {
      id
      name
      description
      website
      imageUrl
      allTimeDonations
      thisYearDonations
    }
  }
`;
exports.GET_PARTNERS_QUERY = GET_PARTNERS_QUERY;

const useGetPlanetPartners = () => {
  const {
    data,
    error,
    loading,
    refetch
  } = (0, _client.useQuery)(GET_PARTNERS_QUERY);
  return {
    error,
    loading,
    partners: data && data.getCharities || [],
    refetch
  };
};

exports.useGetPlanetPartners = useGetPlanetPartners;