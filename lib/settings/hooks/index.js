"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useGetPublicSettings = require("./useGetPublicSettings");

Object.keys(_useGetPublicSettings).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _useGetPublicSettings[key];
    }
  });
});