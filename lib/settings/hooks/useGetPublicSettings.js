"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGetPublicSettings = exports.GET_PUBLIC_SETTINGS_QUERY = void 0;

var _client = require("@apollo/client");

const GET_PUBLIC_SETTINGS_QUERY = (0, _client.gql)`
  query GetPublicSettings {
    publicSettings {
      name
      value
    }
  }
`;
exports.GET_PUBLIC_SETTINGS_QUERY = GET_PUBLIC_SETTINGS_QUERY;

const useGetPublicSettings = () => {
  const {
    data,
    error,
    loading
  } = (0, _client.useQuery)(GET_PUBLIC_SETTINGS_QUERY);
  let preOrderMode;
  let contactEmail;
  let contactPhone;
  (data && data.publicSettings || []).forEach(({
    name,
    value
  }) => {
    switch (name) {
      case 'CONTACT_EMAIL':
        contactEmail = value;
        break;

      case 'CONTACT_PHONE':
        contactPhone = value;
        break;

      case 'PRE_ORDER_MODE':
        preOrderMode = value === 'true';
        break;

      default:
        break;
    }
  });
  return {
    error,
    loading,
    preOrderMode,
    contactEmail,
    contactPhone
  };
};

exports.useGetPublicSettings = useGetPublicSettings;