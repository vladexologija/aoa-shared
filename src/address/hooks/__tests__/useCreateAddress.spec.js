/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CREATE_ADDRESS_MUTATION } from '../useCreateAddress';

describe('Test CREATE_ADDRESS_MUTATION Mutation', () => {
  it('Calling the mutation with address and skipAddressValidation=false makes a valid mutation', () => {
    schemaTestValidQuery(CREATE_ADDRESS_MUTATION, {
      input: {
        address: {
          fullName: 'John Doe',
          line1: 'Street 1',
          city: 'New York',
          state: 'New York',
          zipCode: '10105',
          phoneNumber: '212-702-9054',
          default: false,
        },
        skipAddressValidation: false,
      },
    });
  });

  it('Calling the mutation with address and skipAddressValidation=true makes a valid mutation', () => {
    schemaTestValidQuery(CREATE_ADDRESS_MUTATION, {
      input: {
        address: {
          fullName: 'John Doe',
          line1: 'Street 1',
          city: 'New York',
          state: 'New York',
          zipCode: '10105',
          phoneNumber: '212-702-9054',
          default: true,
        },
        skipAddressValidation: true,
      },
    });
  });
});
