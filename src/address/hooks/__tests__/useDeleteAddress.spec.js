/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { DELETE_ADDRESS } from '../useDeleteAddress';

describe('Test DELETE_ADDRESS Mutation', () => {
  it('Calling the mutation with address id makes a valid mutation', () => {
    schemaTestValidQuery(DELETE_ADDRESS, {
      input: {
        addressId: '20',
      },
    });
  });

  it('Calling the mutation with address id / customer id makes a valid mutation', () => {
    schemaTestValidQuery(DELETE_ADDRESS, {
      input: {
        addressId: '20',
        customerId: '20',
      },
    });
  });
});
