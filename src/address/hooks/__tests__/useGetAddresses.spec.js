/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_ADDRESSES_QUERY } from '../useGetAddresses';

describe('Test GET_ADDRESSES_QUERY', () => {
  it('Calling the query without params makes valid query', () => schemaTestValidQuery(GET_ADDRESSES_QUERY));

  it('Calling the query with customer id makes a valid query', () =>
    schemaTestValidQuery(GET_ADDRESSES_QUERY, {
      customerId: '40',
    }));
});
