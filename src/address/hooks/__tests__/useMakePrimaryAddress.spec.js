/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { MAKE_PRIMARY_ADDRESS_MUTATION } from '../useMakePrimaryAddress';

describe('Test MAKE_PRIMARY_ADDRESS_MUTATION Mutation', () => {
  it('Calling the Mutation with an addressId makes a valid Mutation', () => {
    schemaTestValidQuery(MAKE_PRIMARY_ADDRESS_MUTATION, {
      input: {
        addressId: '5',
      },
    });
  });

  it('Calling the Mutation with an addressId and customerId makes a valid Mutation', () => {
    schemaTestValidQuery(MAKE_PRIMARY_ADDRESS_MUTATION, {
      input: {
        addressId: '5',
        customerId: '10',
      },
    });
  });
});
