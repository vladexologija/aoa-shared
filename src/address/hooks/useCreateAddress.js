import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { addressFragment } from 'common/fragments';
import { GET_ADDRESSES_QUERY } from './useGetAddresses';

export const CREATE_ADDRESS_MUTATION = gql`
  mutation CreateAddress($input: CreateAddressInput!) {
    createAddress(input: $input) {
      address {
        ...AddressFields
        phoneNumber
        deliveryInstructions
        default
      }
      isValidForShipping
      candidates {
        line1
        line2
        city
        state
        zipCode
      }
    }
  }
  ${addressFragment}
`;

export const useCreateAddress = () => {
  const [createAddress, { data, loading, error }] = useMutation(CREATE_ADDRESS_MUTATION, {
    update(cache, { data: mutationResult }) {
      const createdAddress = get(mutationResult, 'createAddress.address');

      if (createdAddress) {
        const { addresses: cachedAddresses } = cache.readQuery({ query: GET_ADDRESSES_QUERY });

        const addressArray = [];

        // if the created address is the primary, make sure the older primary is removed
        (cachedAddresses || []).forEach((address) => {
          addressArray.push(createdAddress.default ? { ...address, default: false } : address);
        });

        addressArray.push(createdAddress);

        cache.writeQuery({
          query: GET_ADDRESSES_QUERY,
          data: { addresses: addressArray },
        });
      }
    },
  });

  const createAddressCallback = useCallback(
    (address, skipAddressValidation = false) => {
      createAddress({
        variables: {
          input: { address, skipAddressValidation },
        },
      }).catch(() => null);
    },
    [createAddress],
  );

  return {
    createAddress: createAddressCallback,
    loading,
    error,
    address: get(data, 'createAddress.address'),
    isValidForShipping: get(data, 'createAddress.isValidForShipping'),
    candidates: (get(data, 'createAddress.candidates') || []).map((candidate) => ({ ...candidate, id: Object.values(candidate).join() })),
  };
};
