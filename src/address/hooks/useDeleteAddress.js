import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import { GET_ADDRESSES_QUERY } from './useGetAddresses';

export const DELETE_ADDRESS = gql`
  mutation DeleteAddress($input: DeleteAddressInput!) {
    deleteAddress(input: $input)
  }
`;

export const useDeleteAddress = () => {
  const [deleteAddress, { data, loading, error }] = useMutation(DELETE_ADDRESS, {
    update: (cache, { data: mutationResult }) => {
      const { addresses } = cache.readQuery({ query: GET_ADDRESSES_QUERY });

      cache.writeQuery({
        query: GET_ADDRESSES_QUERY,
        data: { addresses: (addresses || []).filter((address) => address.id !== mutationResult.deleteAddress) },
      });
    },
  });

  const deleteAddressCallback = useCallback(
    (addressId) => {
      deleteAddress({
        variables: {
          input: { addressId },
        },
      }).catch(() => null);
    },
    [deleteAddress],
  );

  return {
    deleteAddress: deleteAddressCallback,
    loading,
    error,
    address: data && data.deleteAddress,
  };
};
