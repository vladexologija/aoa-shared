import { gql, useQuery } from '@apollo/client';
import get from 'lodash/get';
import { addressFragment } from 'common/fragments';

export const GET_ADDRESSES_QUERY = gql`
  query GetAddresses($customerId: ID) {
    addresses(customerId: $customerId) {
      ...AddressFields
      phoneNumber
      default
    }
  }
  ${addressFragment}
`;

export const useGetAddresses = (customerId) => {
  const { loading, data, error, refetch } = useQuery(GET_ADDRESSES_QUERY, {
    variables: { customerId },
  });

  return {
    loading,
    error,
    addresses: get(data, 'addresses', []),
    refetch,
  };
};
