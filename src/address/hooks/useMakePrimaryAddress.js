import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { GET_ADDRESSES_QUERY } from './useGetAddresses';

export const MAKE_PRIMARY_ADDRESS_MUTATION = gql`
  mutation MakePrimaryAddress($input: MakeAddressPrimaryInput!) {
    makePrimaryAddress(input: $input) {
      id
    }
  }
`;

export const useMakePrimaryAddress = () => {
  const [makePrimaryAddress, { data, loading, error }] = useMutation(MAKE_PRIMARY_ADDRESS_MUTATION, {
    update: (cache, { data: mutationResult }) => {
      const primaryAddressId = get(mutationResult, 'makePrimaryAddress.id');

      if (primaryAddressId) {
        const { addresses } = cache.readQuery({ query: GET_ADDRESSES_QUERY });

        cache.writeQuery({
          query: GET_ADDRESSES_QUERY,
          data: { addresses: (addresses || []).map((address) => ({ ...address, default: address.id === primaryAddressId })) },
        });
      }
    },
  });

  const makePrimaryAddressCallback = useCallback(
    (addressId) => {
      makePrimaryAddress({
        variables: {
          input: { addressId },
        },
      }).catch(() => null);
    },
    [makePrimaryAddress],
  );

  return {
    makePrimaryAddress: makePrimaryAddressCallback,
    loading,
    error,
    data: data && data.makePrimaryAddress,
  };
};
