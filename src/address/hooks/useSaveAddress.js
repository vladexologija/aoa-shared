import { useCreateAddress } from './useCreateAddress';
import { useUpdateAddress } from './useUpdateAddress';

export const useSaveAddress = (newAddress) => {
  const {
    createAddress,
    loading: createLoading,
    error: createError,
    address: addressCreated,
    isValidForShipping: createIsValidForShipping,
    candidates: createCandidates,
  } = useCreateAddress();

  const {
    updateAddress,
    loading: updateLoading,
    error: updateError,
    address: addressUpdated,
    isValidForShipping: updateIsValidForShipping,
    candidates: updateCandidates,
  } = useUpdateAddress();

  return newAddress
    ? {
        saveAddress: createAddress,
        loading: createLoading,
        error: createError,
        address: addressCreated,
        isValidForShipping: createIsValidForShipping,
        candidates: createCandidates,
      }
    : {
        saveAddress: updateAddress,
        loading: updateLoading,
        error: updateError,
        address: addressUpdated,
        isValidForShipping: updateIsValidForShipping,
        candidates: updateCandidates,
      };
};
