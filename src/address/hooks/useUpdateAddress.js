import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { addressFragment } from 'common/fragments';

export const UPDATE_ADDRESS_MUTATION = gql`
  mutation UpdateAddress($input: UpdateAddressInput!) {
    updateAddress(input: $input) {
      address {
        ...AddressFields
        phoneNumber
        deliveryInstructions
        default
      }
      isValidForShipping
      candidates {
        line1
        line2
        city
        state
        zipCode
      }
    }
  }
  ${addressFragment}
`;

export const useUpdateAddress = () => {
  const [updateAddress, { data, loading, error }] = useMutation(UPDATE_ADDRESS_MUTATION);

  const updateAddressCallback = useCallback(
    (address, skipAddressValidation = false) => {
      updateAddress({
        variables: {
          input: { address, skipAddressValidation },
        },
      }).catch(() => null);
    },
    [updateAddress],
  );

  return {
    updateAddress: updateAddressCallback,
    loading,
    error,
    address: get(data, 'updateAddress.address'),
    isValidForShipping: get(data, 'updateAddress.isValidForShipping'),
    candidates: (get(data, 'updateAddress.candidates') || []).map((candidate) => ({ ...candidate, id: Object.values(candidate).join() })),
  };
};
