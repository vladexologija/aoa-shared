/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CHANGE_PASSWORD_MUTATION } from '../useChangePassword';

describe('Test CHANGE_PASSWORD_MUTATION Mutation', () => {
  it('Calling the mutation with newPassword and token makes a valid mutation', () => {
    schemaTestValidQuery(CHANGE_PASSWORD_MUTATION, {
      newPassword: 'new-pass',
      token: 'change-pwd-token',
    });
  });
});
