/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CHECK_PASSWORD_RESET_TOKEN_MUTATION } from '../useCheckResetPasswordToken';

describe('Test CHECK_PASSWORD_RESET_TOKEN_MUTATION Mutation', () => {
  it('Calling the mutation with token makes a valid mutation', () => {
    schemaTestValidQuery(CHECK_PASSWORD_RESET_TOKEN_MUTATION, {
      input: { token: 'change-pwd-token' },
    });
  });
});
