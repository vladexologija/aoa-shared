/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CONFIRM_REGISTER_EMAIL_MUTATION } from '../useConfirmRegisterEmail';

describe('Test CONFIRM_REGISTER_EMAIL_MUTATION Mutation', () => {
  it('Calling the mutation with token makes a valid mutation', () => {
    schemaTestValidQuery(CONFIRM_REGISTER_EMAIL_MUTATION, {
      token: 'token',
    });
  });
});
