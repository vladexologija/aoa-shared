/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CONFIRM_EXISTS_CUSTOMER_EMAIL } from '../useExistsCustomerEmail';

describe('Test CONFIRM_EXISTS_CUSTOMER_EMAIL Query', () => {
  it('Calling the query with email makes a valid query', () => {
    schemaTestValidQuery(CONFIRM_EXISTS_CUSTOMER_EMAIL, {
      email: 'app@aceofair.com',
    });
  });
});
