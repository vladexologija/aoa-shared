/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { LOGIN_MUTATION } from '../useLogin';

describe('Test LOGIN_MUTATION Query', () => {
  it('Calling the query with email and password makes a valid mutation', () => {
    schemaTestValidQuery(LOGIN_MUTATION, {
      email: 'app@aceofair.com',
      password: 'pwd',
    });
  });
});
