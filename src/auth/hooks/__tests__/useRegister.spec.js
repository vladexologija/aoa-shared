/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { REGISTER_MUTATION } from '../useRegister';

describe('Test REGISTER_MUTATION Query', () => {
  it('Calling the query with all the parameters makes a valid mutation', () => {
    schemaTestValidQuery(REGISTER_MUTATION, {
      user: {
        firstName: 'Test',
        lastName: 'Lastname',
        email: 'app@aceofair.com',
        password: 'pwd',
        marketingOptIn: true,
        invitationCode: 'AABB',
        acceptedTerms: true,
      },
    });
  });
});
