/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { RESET_PASSWORD_MUTATION } from '../useResetPassword';

describe('Test RESET_PASSWORD_MUTATION Query', () => {
  it('Calling the query with email makes a valid mutation', () => {
    schemaTestValidQuery(RESET_PASSWORD_MUTATION, {
      email: 'app@aceofair.com',
    });
  });
});
