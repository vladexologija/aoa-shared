/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { WAITLIST_MUTATION } from '../useSaveLead';

describe('Test WAITLIST_MUTATION Query', () => {
  it('Calling the query with email makes a valid mutation', () => {
    schemaTestValidQuery(WAITLIST_MUTATION, {
      email: 'app@aceofair.com',
    });
  });
});
