/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { SIGNUP_NEWSLETTER_MUTATION } from '../useSignupNewsletter';

describe('Test SIGNUP_NEWSLETTER_MUTATION Query', () => {
  it('Calling the query with email makes a valid mutation', () => {
    schemaTestValidQuery(SIGNUP_NEWSLETTER_MUTATION, {
      email: 'app@aceofair.com',
    });
  });
});
