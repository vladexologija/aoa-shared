/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { USER_LOGIN_MUTATION } from '../useUserLogin';

describe('Test USER_LOGIN_MUTATION Query', () => {
  it('Calling the query with username and password makes a valid mutation', () => {
    schemaTestValidQuery(USER_LOGIN_MUTATION, {
      username: 'app@aceofair.com',
      password: 'pwd',
    });
  });
});
