import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const CHANGE_PASSWORD_MUTATION = gql`
  mutation ChangePassword($newPassword: String!, $token: String!) {
    changePassword(newPassword: $newPassword, token: $token)
  }
`;

export const useChangePassword = () => {
  const [changePassword, { data, loading, error }] = useMutation(CHANGE_PASSWORD_MUTATION);

  const changePasswordCallback = useCallback(
    (newPassword, token) => {
      changePassword({ variables: { newPassword, token } }).catch(() => null);
    },
    [changePassword],
  );

  return {
    changePassword: changePasswordCallback,
    loading,
    error,
    data: data && data.changePassword,
  };
};
