import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const CHECK_PASSWORD_RESET_TOKEN_MUTATION = gql`
  mutation CheckResetPasswordToken($input: CheckTokenInput!) {
    checkResetPasswordToken(input: $input)
  }
`;

export const useCheckResetPasswordToken = () => {
  const [checkResetPasswordToken, { data, loading, error }] = useMutation(CHECK_PASSWORD_RESET_TOKEN_MUTATION);

  const checkResetPasswordTokenCallback = useCallback(
    (token) => {
      checkResetPasswordToken({ variables: { input: { token } } }).catch(() => null);
    },
    [checkResetPasswordToken],
  );

  return {
    checkResetPasswordToken: checkResetPasswordTokenCallback,
    loading,
    error,
    data: data && data.checkResetPasswordToken,
  };
};
