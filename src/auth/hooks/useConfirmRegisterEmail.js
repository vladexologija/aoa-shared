import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const CONFIRM_REGISTER_EMAIL_MUTATION = gql`
  mutation ConfirmRegisterEmail($token: String!) {
    confirmRegisterEmail(token: $token) {
      accessToken
    }
  }
`;

export const useConfirmRegisterEmail = () => {
  const [confirmRegisterEmail, { data, loading, error }] = useMutation(CONFIRM_REGISTER_EMAIL_MUTATION);

  const confirmRegisterEmailCallback = useCallback(
    (token) => {
      confirmRegisterEmail({ variables: { token } }).catch(() => null);
    },
    [confirmRegisterEmail],
  );

  return {
    confirmRegisterEmail: confirmRegisterEmailCallback,
    loading,
    error,
    data: data && data.confirmRegisterEmail,
  };
};
