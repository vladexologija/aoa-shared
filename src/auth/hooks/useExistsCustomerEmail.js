import { gql, useQuery } from '@apollo/client';

export const CONFIRM_EXISTS_CUSTOMER_EMAIL = gql`
  query ExistsCustomerEmail($email: String!) {
    existsCustomerEmail(email: $email)
  }
`;

export const useExistsCustomerEmail = (email) => {
  const { loading, error, data } = useQuery(CONFIRM_EXISTS_CUSTOMER_EMAIL, {
    variables: { email },
  });

  return {
    loading,
    error,
    data: data && data.existsCustomerEmail,
  };
};
