import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const LOGIN_MUTATION = gql`
  mutation CustomerLogin($email: String!, $password: String!) {
    customerLogin(email: $email, password: $password) {
      accessToken
    }
  }
`;

export const useLogin = () => {
  const [customerLogin, { data, loading, error }] = useMutation(LOGIN_MUTATION);

  const customerLoginCallback = useCallback(
    (email, password) => {
      customerLogin({ variables: { email, password } }).catch(() => null);
    },
    [customerLogin],
  );

  return {
    customerLogin: customerLoginCallback,
    loading,
    error,
    data: data && data.customerLogin,
  };
};
