import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const REGISTER_MUTATION = gql`
  mutation RegisterUser($user: CustomerRegister!) {
    customerRegister(customer: $user)
  }
`;

export const useRegister = () => {
  const [customerRegister, { data, loading, error }] = useMutation(REGISTER_MUTATION);

  const customerRegisterCallback = useCallback(
    (firstName, lastName, email, password, marketingOptIn, invitationCode, acceptedTerms) => {
      customerRegister({ variables: { user: { firstName, lastName, email, password, marketingOptIn, invitationCode, acceptedTerms } } }).catch(
        () => null,
      );
    },
    [customerRegister],
  );

  return {
    customerRegister: customerRegisterCallback,
    loading,
    error,
    data: data && data.customerRegister,
  };
};
