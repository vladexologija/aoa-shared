import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const RESET_PASSWORD_MUTATION = gql`
  mutation ResetPassword($email: String!) {
    resetPassword(email: $email)
  }
`;

export const useResetPassword = () => {
  const [resetPassword, { data, loading, error }] = useMutation(RESET_PASSWORD_MUTATION);

  const resetPasswordCallback = useCallback(
    (email) => {
      resetPassword({ variables: { email } }).catch(() => null);
    },
    [resetPassword],
  );

  return {
    resetPassword: resetPasswordCallback,
    loading,
    error,
    data: data && data.resetPassword,
  };
};
