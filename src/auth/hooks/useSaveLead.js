import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const WAITLIST_MUTATION = gql`
  mutation SaveLead($email: String!) {
    saveLead(email: $email)
  }
`;

export const useSaveLead = () => {
  const [saveLead, { data, loading, error }] = useMutation(WAITLIST_MUTATION);

  const saveLeadCallback = useCallback(
    (email) => {
      saveLead({ variables: { email } }).catch(() => null);
    },
    [saveLead],
  );

  return {
    saveLead: saveLeadCallback,
    loading,
    error,
    data: data && data.saveLead,
  };
};
