import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const SIGNUP_NEWSLETTER_MUTATION = gql`
  mutation SignupNewsletter($email: String!) {
    signupNewsletter(input: { email: $email }) {
      success
      message
    }
  }
`;

export const useSignupNewsletter = () => {
  const [signupNewsletter, { data, loading, error }] = useMutation(SIGNUP_NEWSLETTER_MUTATION);

  const signupNewsletterCallback = useCallback(
    (email) => {
      signupNewsletter({ variables: { email } }).catch(() => null);
    },
    [signupNewsletter],
  );

  return {
    signupNewsletter: signupNewsletterCallback,
    loading,
    error,
    success: data && data.signupNewsletter && data.signupNewsletter.success,
    message: data && data.signupNewsletter && data.signupNewsletter.message,
  };
};
