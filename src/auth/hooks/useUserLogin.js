import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const USER_LOGIN_MUTATION = gql`
  mutation UserLogin($username: String!, $password: String!) {
    userLogin(username: $username, password: $password) {
      accessToken
      roles
    }
  }
`;

export const useUserLogin = () => {
  const [userLogin, { data, loading, error }] = useMutation(USER_LOGIN_MUTATION);

  const userLoginCallback = useCallback(
    (username, password) => {
      userLogin({ variables: { username, password } }).catch(() => null);
    },
    [userLogin],
  );

  return {
    userLogin: userLoginCallback,
    loading,
    error,
    data: data && data.userLogin,
  };
};
