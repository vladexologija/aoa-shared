/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CANCEL_AUTO_REPLENISH_PLAN_MUTATION } from '../useCancelAutoReplenishPlan';

describe('Test CANCEL_AUTO_REPLENISH_PLAN_MUTATION Mutation', () => {
  it('Calling the mutation with id makes a valid mutation', () => {
    schemaTestValidQuery(CANCEL_AUTO_REPLENISH_PLAN_MUTATION, {
      id: '10',
    });
  });
});
