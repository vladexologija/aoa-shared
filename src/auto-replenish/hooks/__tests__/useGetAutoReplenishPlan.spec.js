/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_AUTO_REPLENISH_PLAN_QUERY } from '../useGetAutoReplenishPlan';

describe('Test GET_AUTO_REPLENISH_PLAN_QUERY Mutation', () => {
  it('Calling the query with id makes a valid query', () => {
    schemaTestValidQuery(GET_AUTO_REPLENISH_PLAN_QUERY, {
      id: '10',
    });
  });
});
