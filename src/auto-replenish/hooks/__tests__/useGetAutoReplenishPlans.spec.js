/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_AUTO_REPLENISH_PLANS_QUERY } from '../useGetAutoReplenishPlans';

describe('Test GET_AUTO_REPLENISH_PLANS_QUERY Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_AUTO_REPLENISH_PLANS_QUERY);
  });

  it('Calling the query with customerId makes a valid query', () => {
    schemaTestValidQuery(GET_AUTO_REPLENISH_PLANS_QUERY, {
      customerId: '5',
    });
  });
});
