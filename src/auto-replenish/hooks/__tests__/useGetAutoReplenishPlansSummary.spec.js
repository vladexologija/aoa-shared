/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY } from '../useGetAutoReplenishPlansSummary';

describe('Test GET_AUTO_REPLENISH_PLANS_QUERY Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY);
  });
});
