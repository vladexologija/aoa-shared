/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { PAUSE_AUTO_REPLENISH_PLAN_MUTATION } from '../usePauseAutoReplenishPlan';

describe('Test PAUSE_AUTO_REPLENISH_PLAN_MUTATION Mutation', () => {
  it('Calling the mutation with id makes a valid mutation', () => {
    schemaTestValidQuery(PAUSE_AUTO_REPLENISH_PLAN_MUTATION, {
      id: '10',
    });
  });
});
