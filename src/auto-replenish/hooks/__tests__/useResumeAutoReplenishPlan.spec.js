/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION } from '../useResumeAutoReplenishPlan';

describe('Test UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION Mutation', () => {
  it('Calling the mutation with id / nextDesiredDelivery makes a valid mutation', () => {
    schemaTestValidQuery(UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION, {
      id: '10',
      nextDesiredDelivery: new Date(),
    });
  });
});
