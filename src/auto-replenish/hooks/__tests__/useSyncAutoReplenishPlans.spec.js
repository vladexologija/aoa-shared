/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { SYNC_AUTO_REPLENISH_PLANS_MUTATION } from '../useSyncAutoReplenishPlans';

describe('Test SYNC_AUTO_REPLENISH_PLANS_MUTATION Mutation', () => {
  it('Calling the mutation with ids / desiredDelivery makes a valid mutation', () => {
    schemaTestValidQuery(SYNC_AUTO_REPLENISH_PLANS_MUTATION, {
      ids: ['10', '11'],
      desiredDelivery: new Date(),
    });
  });
});
