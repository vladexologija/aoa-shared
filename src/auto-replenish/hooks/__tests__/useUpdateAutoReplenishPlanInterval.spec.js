/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_AUTO_REPLENISH_PLAN_INTERVAL } from '../useUpdateAutoReplenishPlanInterval';

describe('Test SYNC_AUTO_REPLENISH_PLANS_MUTATION Mutation', () => {
  it('Calling the mutation with ids / desiredDelivery makes a valid mutation', () => {
    schemaTestValidQuery(UPDATE_AUTO_REPLENISH_PLAN_INTERVAL, {
      id: '10',
      interval: 1,
      type: 'MONTH',
    });
  });
});
