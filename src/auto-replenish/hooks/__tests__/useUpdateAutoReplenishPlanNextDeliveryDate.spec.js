/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE } from '../useUpdateAutoReplenishPlanNextDeliveryDate';

describe('Test UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE Mutation', () => {
  it('Calling the mutation with id / date makes a valid mutation', () => {
    schemaTestValidQuery(UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE, {
      id: '10',
      date: new Date(),
    });
  });
});
