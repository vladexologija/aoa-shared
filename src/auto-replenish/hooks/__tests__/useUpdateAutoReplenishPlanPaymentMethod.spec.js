/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_AUTO_REPLENISH_PLAN_METHOD } from '../useUpdateAutoReplenishPlanPaymentMethod';

describe('Test UPDATE_AUTO_REPLENISH_PLAN_METHOD Mutation', () => {
  it('Calling the mutation with id / date makes a valid mutation', () => {
    schemaTestValidQuery(UPDATE_AUTO_REPLENISH_PLAN_METHOD, {
      id: '10',
      paymentMethodId: '20',
    });
  });
});
