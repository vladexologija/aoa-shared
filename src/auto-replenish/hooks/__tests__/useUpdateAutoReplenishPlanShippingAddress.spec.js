/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS } from '../useUpdateAutoReplenishPlanShippingAddress';

describe('Test UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS Mutation', () => {
  it('Calling the mutation with id / date makes a valid mutation', () => {
    schemaTestValidQuery(UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS, {
      id: '10',
      shippingAddressId: '20',
    });
  });
});
