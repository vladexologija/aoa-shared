import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const CANCEL_AUTO_REPLENISH_PLAN_MUTATION = gql`
  mutation CancelAutoReplenishPlan($id: ID!) {
    cancelAutoReplenishPlan(id: $id) {
      id
      canceledAt
      desiredDeliveryAt
    }
  }
`;

export const useCancelAutoReplenishPlan = () => {
  const [cancelAutoReplenishPlan, { data, loading, error }] = useMutation(CANCEL_AUTO_REPLENISH_PLAN_MUTATION);

  const cancelAutoReplenishPlanCallback = useCallback(
    (id) => {
      cancelAutoReplenishPlan({
        variables: {
          id,
        },
      }).catch(() => null);
    },
    [cancelAutoReplenishPlan],
  );

  return {
    cancelAutoReplenishPlan: cancelAutoReplenishPlanCallback,
    loading,
    error,
    data,
  };
};
