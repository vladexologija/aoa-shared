import { gql, useQuery } from '@apollo/client';
import { arPlanFragment, addressFragment, paymentMethodFragment } from 'common/fragments';

export const GET_AUTO_REPLENISH_PLAN_QUERY = gql`
  query GetAutoReplenishPlan($id: ID!) {
    autoReplenishPlan(id: $id) {
      ...AutoReplenishPlanFields
      shippingAddress {
        ...AddressFields
      }
      paymentMethod {
        ...PaymentMethodFields
      }
    }
  }
  ${arPlanFragment}
  ${addressFragment}
  ${paymentMethodFragment}
`;

export const useGetAutoReplenishPlan = (id) => {
  const { loading, error, data } = useQuery(GET_AUTO_REPLENISH_PLAN_QUERY, {
    variables: { id },
  });

  return {
    loading,
    error,
    arPlan: data && data.autoReplenishPlan,
  };
};
