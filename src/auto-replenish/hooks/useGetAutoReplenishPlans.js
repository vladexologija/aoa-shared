import { gql, useQuery } from '@apollo/client';
import { arPlanFragment, addressFragment, paymentMethodFragment } from 'common/fragments';

export const GET_AUTO_REPLENISH_PLANS_QUERY = gql`
  query GetAutoReplenishPlans($customerId: ID) {
    getAutoReplenishPlans(customerId: $customerId) {
      ...AutoReplenishPlanFields
      shippingAddress {
        ...AddressFields
      }
      paymentMethod {
        ...PaymentMethodFields
      }
    }
  }
  ${arPlanFragment}
  ${addressFragment}
  ${paymentMethodFragment}
`;

export const useGetAutoReplenishPlans = (customerId) => {
  const { loading, error, data } = useQuery(GET_AUTO_REPLENISH_PLANS_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: { customerId },
  });

  return {
    loading,
    error,
    arPlans: (data && data.getAutoReplenishPlans) || [],
  };
};
