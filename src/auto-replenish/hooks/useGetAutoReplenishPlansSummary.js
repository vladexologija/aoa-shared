import { gql, useQuery, NetworkStatus } from '@apollo/client';

export const GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY = gql`
  query GetAutoReplenishPlansSummary {
    getAutoReplenishPlans {
      id
      desiredDeliveryAt
      product {
        id
        name
        imageUrl
      }
    }
  }
`;

export const useGetAutoReplenishPlansSummary = () => {
  const { loading, error, data, refetch, networkStatus } = useQuery(GET_AUTO_REPLENISH_PLANS_SUMMARY_QUERY, {
    notifyOnNetworkStatusChange: true,
  });

  return {
    loading: loading || networkStatus === NetworkStatus.refetch,
    error,
    arPlans: (data && data.getAutoReplenishPlans) || [],
    refetch,
  };
};
