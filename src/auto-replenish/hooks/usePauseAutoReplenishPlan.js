import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const PAUSE_AUTO_REPLENISH_PLAN_MUTATION = gql`
  mutation PauseAutoReplenishPlan($id: ID!) {
    pauseAutoReplenishPlan(id: $id) {
      id
      pausedAt
      desiredDeliveryAt
    }
  }
`;

export const usePauseAutoReplenishPlan = () => {
  const [pauseAutoReplenishPlan, { data, loading, error }] = useMutation(PAUSE_AUTO_REPLENISH_PLAN_MUTATION);

  const pauseAutoReplenishPlanCallback = useCallback(
    (id) => {
      pauseAutoReplenishPlan({
        variables: {
          id,
        },
      }).catch(() => null);
    },
    [pauseAutoReplenishPlan],
  );

  return {
    pauseAutoReplenishPlan: pauseAutoReplenishPlanCallback,
    loading,
    error,
    data,
  };
};
