import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION = gql`
  mutation ResumeAutoReplenishPlan($id: ID!, $nextDesiredDelivery: DateTime!) {
    resumeAutoReplenishPlan(id: $id, nextDesiredDelivery: $nextDesiredDelivery) {
      id
      pausedAt
      desiredDeliveryAt
    }
  }
`;

export const useResumeAutoReplenishPlan = () => {
  const [resumeAutoReplenishPlan, { data, loading, error }] = useMutation(UNPAUSE_AUTO_REPLENISH_PLAN_MUTATION);

  const resumeAutoReplenishPlanCallback = useCallback(
    (id, nextDesiredDelivery) => {
      resumeAutoReplenishPlan({
        variables: { id, nextDesiredDelivery },
      }).catch(() => null);
    },
    [resumeAutoReplenishPlan],
  );

  return {
    resumeAutoReplenishPlan: resumeAutoReplenishPlanCallback,
    loading,
    error,
    data,
  };
};
