import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const SYNC_AUTO_REPLENISH_PLANS_MUTATION = gql`
  mutation SyncAutoReplenishPlans($ids: [ID]!, $desiredDelivery: DateTime!) {
    syncAutoReplenishPlans(ids: $ids, desiredDelivery: $desiredDelivery) {
      id
      desiredDeliveryAt
    }
  }
`;

export const useSyncAutoReplenishPlans = () => {
  const [syncAutoReplenishPlans, { data, loading, error }] = useMutation(SYNC_AUTO_REPLENISH_PLANS_MUTATION);

  const syncAutoReplenishPlansCallback = useCallback(
    (ids, desiredDelivery) => {
      syncAutoReplenishPlans({
        variables: { ids, desiredDelivery },
      }).catch(() => null);
    },
    [syncAutoReplenishPlans],
  );

  return {
    syncAutoReplenishPlans: syncAutoReplenishPlansCallback,
    loading,
    error,
    data,
  };
};
