import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const UPDATE_AUTO_REPLENISH_PLAN_INTERVAL = gql`
  mutation UpdateAutoReplenishPlanInterval($id: ID!, $interval: Int!, $type: AutoReplenishIntervalType!) {
    updateAutoReplenishPlanInterval(id: $id, interval: $interval, type: $type) {
      id
      interval
      desiredDeliveryAt
    }
  }
`;

export const useUpdateAutoReplenishPlanInterval = () => {
  const [updateAutoReplenishPlanInterval, { data, loading, error }] = useMutation(UPDATE_AUTO_REPLENISH_PLAN_INTERVAL);

  const updateCallback = useCallback(
    (id, interval, type = 'MONTH') => {
      updateAutoReplenishPlanInterval({
        variables: {
          id,
          interval,
          type,
        },
      }).catch(() => null);
    },
    [updateAutoReplenishPlanInterval],
  );

  return {
    updateAutoReplenishPlanInterval: updateCallback,
    loading,
    error,
    data,
  };
};
