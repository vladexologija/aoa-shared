import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE = gql`
  mutation UpdateAutoReplenishPlanNextDeliveryDate($id: ID!, $date: DateTime!) {
    updateAutoReplenishPlanNextDeliveryDate(id: $id, date: $date) {
      id
      desiredDeliveryAt
    }
  }
`;

export const useUpdateAutoReplenishPlanNextDeliveryDate = () => {
  const [updateAutoReplenishPlanNextDeliveryDate, { data, loading, error }] = useMutation(UPDATE_AUTO_REPLENISH_PLAN_NEXT_DELIVERY_DATE);

  const updateCallback = useCallback(
    (id, date) => {
      updateAutoReplenishPlanNextDeliveryDate({
        variables: { id, date },
      }).catch(() => null);
    },
    [updateAutoReplenishPlanNextDeliveryDate],
  );

  return {
    updateAutoReplenishPlanNextDeliveryDate: updateCallback,
    loading,
    error,
    data,
  };
};
