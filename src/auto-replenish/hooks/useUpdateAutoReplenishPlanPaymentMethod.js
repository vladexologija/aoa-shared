import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const UPDATE_AUTO_REPLENISH_PLAN_METHOD = gql`
  mutation UpdateAutoReplenishPlanPaymentMethod($id: ID!, $paymentMethodId: ID!) {
    updateAutoReplenishPlanPaymentMethod(id: $id, paymentMethodId: $paymentMethodId) {
      id
      paymentMethod {
        id
        brand
        last4Digits
      }
    }
  }
`;

export const useUpdateAutoReplenishPlanPaymentMethod = () => {
  const [updateAutoReplenishPlanPaymentMethod, { data, loading, error }] = useMutation(UPDATE_AUTO_REPLENISH_PLAN_METHOD);

  const updateCallback = useCallback(
    (id, paymentMethodId) => {
      updateAutoReplenishPlanPaymentMethod({
        variables: { id, paymentMethodId },
      }).catch(() => null);
    },
    [updateAutoReplenishPlanPaymentMethod],
  );

  return {
    updateAutoReplenishPlanPaymentMethod: updateCallback,
    loading,
    error,
    data,
  };
};
