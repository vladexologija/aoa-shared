import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS = gql`
  mutation UpdateAutoReplenishPlanShippingAddress($id: ID!, $shippingAddressId: ID!) {
    updateAutoReplenishPlanShippingAddress(id: $id, shippingAddressId: $shippingAddressId) {
      id
      shippingAddress {
        id
        line1
        line2
        city
        state
        zipCode
      }
    }
  }
`;

export const useUpdateAutoReplenishPlanShippingAddress = () => {
  const [updateAutoReplenishPlanShippingAddress, { data, loading, error }] = useMutation(UPDATE_AUTO_REPLENISH_PLAN_SHIPPING_ADDRESS);

  const updateCallback = useCallback(
    (id, shippingAddressId) => {
      updateAutoReplenishPlanShippingAddress({
        variables: { id, shippingAddressId },
      }).catch(() => null);
    },
    [updateAutoReplenishPlanShippingAddress],
  );

  return {
    updateAutoReplenishPlanShippingAddress: updateCallback,
    loading,
    error,
    data: data && data.updateAutoReplenishPlanShippingAddress,
  };
};
