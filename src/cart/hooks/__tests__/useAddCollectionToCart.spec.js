/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { ADD_COLLECTION_TO_CART } from '../useAddCollectionToCart';

describe('Test ADD_COLLECTION_TO_CART Mutation', () => {
  it('Calling the mutation with collectionId makes a valid mutation', () => {
    schemaTestValidQuery(ADD_COLLECTION_TO_CART, {
      input: {
        collectionId: '10',
      },
    });
  });
});
