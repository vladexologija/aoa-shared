/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { ADD_SKU_TO_CART_MUTATION } from '../useAddSkuToCart';

describe('Test ADD_SKU_TO_CART_MUTATION Mutation', () => {
  it('Calling the mutation with skuId/quantity/autoReplenishPlan makes a valid mutation', () => {
    schemaTestValidQuery(ADD_SKU_TO_CART_MUTATION, {
      input: {
        skuId: '10',
        quantity: 10,
        autoReplenishPlan: {
          interval: 1,
          intervalType: 'MONTH',
        },
      },
    });
  });
});
