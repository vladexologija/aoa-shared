/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { APPLY_PROMO_TO_CART_MUTATION } from '../useApplyPromoToCart';

describe('Test APPLY_PROMO_TO_CART_MUTATION Mutation', () => {
  it('Calling the mutation with promoCode makes a valid mutation', () => {
    schemaTestValidQuery(APPLY_PROMO_TO_CART_MUTATION, {
      input: {
        promoCode: 'PROMO10',
      },
    });
  });
});
