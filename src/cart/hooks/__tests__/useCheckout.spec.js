/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_CHECKOUT_QUERY } from '../useCheckout';

describe('Test GET_CHECKOUT_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_CHECKOUT_QUERY);
  });
});
