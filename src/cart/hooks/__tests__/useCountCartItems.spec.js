/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { COUNT_CART_ITEMS_QUERY } from '../useCountCartItems';

describe('Test COUNT_CART_ITEMS_QUERY Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(COUNT_CART_ITEMS_QUERY);
  });
});
