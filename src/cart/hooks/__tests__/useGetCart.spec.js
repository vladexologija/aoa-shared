/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_CART_QUERY } from '../useGetCart';

describe('Test GET_CART_QUERY Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_CART_QUERY);
  });
});
