/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { REMOVE_ITEM_FROM_CART_MUTATION } from '../useRemoveItemFromCart';

describe('Test REMOVE_PROMO_FROM_CART_MUTATION Mutation', () => {
  it('Calling the mutation without parameters makes a valid query', () => {
    schemaTestValidQuery(REMOVE_ITEM_FROM_CART_MUTATION, {
      input: {
        id: '10',
      },
    });
  });
});
