/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { REMOVE_PROMO_FROM_CART_MUTATION } from '../useRemovePromoFromCart';

describe('Test REMOVE_PROMO_FROM_CART_MUTATION Mutation', () => {
  it('Calling the mutation without parameters makes a valid query', () => {
    schemaTestValidQuery(REMOVE_PROMO_FROM_CART_MUTATION);
  });
});
