/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { SET_ADDRESS_TO_CART_MUTATION } from '../useSetAddressToCart';

describe('Test SET_ADDRESS_TO_CART_MUTATION Mutation', () => {
  it('Calling the mutation without parameters makes a valid query', () => {
    schemaTestValidQuery(SET_ADDRESS_TO_CART_MUTATION, {
      input: {
        id: '10',
      },
    });
  });
});
