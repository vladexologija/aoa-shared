/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_CART_ITEM_MUTATION } from '../useUpdateCartItem';

describe('Test SET_ADDRESS_TO_CART_MUTATION Mutation', () => {
  it('Calling the mutation without parameters makes a valid query', () => {
    schemaTestValidQuery(UPDATE_CART_ITEM_MUTATION, {
      input: {
        id: '10',
        quantity: 5,
        autoReplenishPlan: {
          interval: 5,
          intervalType: 'MONTH',
        },
      },
    });
  });
});
