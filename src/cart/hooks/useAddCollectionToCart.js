import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';
import { COUNT_CART_ITEMS_QUERY } from './useCountCartItems';

export const ADD_COLLECTION_TO_CART = gql`
  mutation AddCollectionToCart($input: AddCollectionToCartInput!) {
    addCollectionToCart(input: $input) {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useAddCollectionToCart = () => {
  const [addCollectionToCart, { data, loading, error }] = useMutation(ADD_COLLECTION_TO_CART, {
    update(cache, { data: mutationResult }) {
      const { getCart } = cache.readQuery({ query: COUNT_CART_ITEMS_QUERY });

      if (getCart === null) {
        const cartData = get(mutationResult, 'addCollectionToCart');

        if (cartData) {
          cache.writeQuery({
            query: COUNT_CART_ITEMS_QUERY,
            data: { getCart: cartData },
          });
        }
      }
    },
  });

  const addCollectionToCartCallback = useCallback(
    (collectionId) => {
      addCollectionToCart({
        variables: {
          input: {
            collectionId,
          },
        },
      }).catch(() => null);
    },
    [addCollectionToCart],
  );

  return {
    addCollectionToCart: addCollectionToCartCallback,
    loading,
    error,
    cart: get(data, 'addCollectionToCart.cart'),
    warnings: get(data, 'addCollectionToCart.warnings'),
  };
};
