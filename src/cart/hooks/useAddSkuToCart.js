import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';
import { COUNT_CART_ITEMS_QUERY } from './useCountCartItems';

export const ADD_SKU_TO_CART_MUTATION = gql`
  mutation AddSkuToCartMutation($input: AddSkuToCartInput!) {
    addSkuToCart(input: $input) {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useAddSkuToCart = () => {
  const [addSkuToCart, { data, loading, error }] = useMutation(ADD_SKU_TO_CART_MUTATION, {
    update(cache, { data: mutationResult }) {
      const { getCart } = cache.readQuery({ query: COUNT_CART_ITEMS_QUERY });

      if (getCart === null) {
        const cartData = get(mutationResult, 'addSkuToCart');

        if (cartData) {
          cache.writeQuery({
            query: COUNT_CART_ITEMS_QUERY,
            data: { getCart: cartData },
          });
        }
      }
    },
  });

  const addSkuToCartCallback = useCallback(
    ({ skuId, quantity = 1, interval = 0 }) => {
      addSkuToCart({
        variables: {
          input: {
            skuId,
            quantity,
            autoReplenishPlan: interval > 0 ? { interval, intervalType: 'MONTH' } : null,
          },
        },
      }).catch(() => null);
    },
    [addSkuToCart],
  );

  return {
    addSkuToCart: addSkuToCartCallback,
    loading,
    error,
    cart: get(data, 'addSkuToCart.cart'),
    warnings: get(data, 'addSkuToCart.warnings'),
  };
};
