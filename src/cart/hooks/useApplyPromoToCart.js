import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';

export const APPLY_PROMO_TO_CART_MUTATION = gql`
  mutation ApplyPromoToCartMutation($input: ApplyPromoToCartInput!) {
    applyPromoToCart(input: $input) {
      cart {
        ...CartFields
        grandTotal
      }
      promotion {
        code
        discount
        isValid
        type
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useApplyPromoToCart = () => {
  const [applyPromoToCart, { data, loading, error }] = useMutation(APPLY_PROMO_TO_CART_MUTATION);

  const applyPromoToCartCallback = useCallback(
    (promoCode) => {
      applyPromoToCart({
        variables: {
          input: {
            promoCode,
          },
        },
      }).catch(() => null);
    },
    [applyPromoToCart],
  );

  return {
    applyPromoToCart: applyPromoToCartCallback,
    loading,
    error,
    cart: get(data, 'applyPromoToCart.cart'),
    promotion: get(data, 'applyPromoToCart.promotion'),
    warnings: get(data, 'applyPromoToCart.warnings'),
  };
};
