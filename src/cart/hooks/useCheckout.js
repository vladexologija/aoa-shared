import { useMemo } from 'react';
import { gql, useLazyQuery, NetworkStatus } from '@apollo/client';
import get from 'lodash/get';
import has from 'lodash/has';
import dayjs from 'dayjs';
import { cartFragment } from 'common/fragments';
import { formatPrice } from 'common/utils';

export const GET_CHECKOUT_QUERY = gql`
  query GetCheckout {
    getCart {
      cart {
        address {
          id
          fullName
          line1
          line2
          city
          state
          zipCode
        }
        shipping {
          service
          totalCost
          expectedArrival
        }
        taxes {
          amountToCollect
          rate
        }
        ...CartFields
        grandTotal
      }
      promotion {
        code
        discount
        isValid
        type
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useCheckout = () => {
  const [getCheckout, { loading, data, error, refetch, networkStatus }] = useLazyQuery(GET_CHECKOUT_QUERY, {
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange: true,
  });

  const { cart, warnings, promotion, hasAddress, formattedSubtotals } = useMemo(() => {
    const promotionObject = get(data, 'getCart.promotion');
    const cartObject = get(data, 'getCart.cart');
    const eta = get(cartObject, 'shipping.expectedArrival');

    return {
      cart: cartObject,
      warnings: get(data, 'getCart.warnings'),
      promotion: promotionObject && { ...promotionObject, discount: formatPrice(promotionObject.discount) },
      hasAddress: has(cartObject, ['address', 'id']),
      formattedSubtotals: {
        shippingCost: formatPrice(get(cartObject, 'shipping.totalCost')),
        taxes: formatPrice(get(cartObject, 'taxes.amountToCollect')),
        grandTotal: formatPrice(get(cartObject, 'grandTotal')),
        discountSubtotal: formatPrice(get(cartObject, 'discountSubtotal'), true),
        rentalFees: formatPrice(get(cartObject, 'additionalFees.rentalTotal')),
        eta: eta ? dayjs(eta).format('MMMM D') : '...',
        subtotal: formatPrice(get(cartObject, 'subtotal')),
      },
    };
  }, [data]);

  return {
    getCheckout,
    loading: loading || networkStatus === NetworkStatus.refetch,
    error,
    cart,
    warnings,
    promotion,
    hasAddress,
    formattedSubtotals,
    refetch,
  };
};
