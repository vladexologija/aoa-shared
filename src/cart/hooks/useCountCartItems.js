import { gql, useQuery } from '@apollo/client';
import get from 'lodash/get';

export const COUNT_CART_ITEMS_QUERY = gql`
  query countCartItems {
    getCart {
      cart {
        id
        itemsCount
      }
    }
  }
`;

export const useCountCartItems = () => {
  const { loading, data, error, refetch } = useQuery(COUNT_CART_ITEMS_QUERY);

  return {
    refetch,
    loading,
    itemsCount: get(data, 'getCart.cart.itemsCount', 0),
    error,
  };
};
