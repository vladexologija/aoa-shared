import { useMemo } from 'react';
import { gql, useQuery, NetworkStatus } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';
import { formatPrice } from 'common/utils';

export const GET_CART_QUERY = gql`
  query GetCart {
    getCart {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useGetCart = () => {
  const { loading, data, error, refetch, networkStatus } = useQuery(GET_CART_QUERY, { notifyOnNetworkStatusChange: true });

  const { cart, formattedSubtotals } = useMemo(() => {
    if (data) {
      const cartObject = get(data, 'getCart.cart', null);

      const subtotals = cartObject && {
        rentalFees: formatPrice(get(cartObject, 'additionalFees.rentalTotal')),
        subtotal: formatPrice(get(cartObject, 'subtotal')),
      };

      return { cart: cartObject, formattedSubtotals: subtotals };
    }

    return { cart: undefined, formattedSubtotals: undefined };
  }, [data]);

  return {
    loading: loading || networkStatus === NetworkStatus.refetch,
    error,
    cart,
    warnings: get(data, 'getCart.warnings'),
    formattedSubtotals,
    refetch,
  };
};
