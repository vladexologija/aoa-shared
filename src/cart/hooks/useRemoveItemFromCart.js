import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';

export const REMOVE_ITEM_FROM_CART_MUTATION = gql`
  mutation RemoveItemFromCart($input: RemoveCartItemFromCartInput!) {
    removeItemFromCart(input: $input) {
      cart {
        ...CartFields
        removedItem {
          sku {
            id
            outOfStock
            buyLimitReached
            maxItemsCanBuy
          }
        }
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useRemoveItemFromCart = () => {
  const [removeItemFromCart, { data, loading, error }] = useMutation(REMOVE_ITEM_FROM_CART_MUTATION);

  const removeItemFromCartCallback = useCallback(
    (id) => {
      removeItemFromCart({
        variables: {
          input: {
            id,
          },
        },
      }).catch(() => null);
    },
    [removeItemFromCart],
  );

  return {
    removeItemFromCart: removeItemFromCartCallback,
    loading,
    error,
    cart: get(data, 'removeItemFromCart.cart'),
    warnings: get(data, 'removeItemFromCart.warnings'),
  };
};
