import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';

export const REMOVE_PROMO_FROM_CART_MUTATION = gql`
  mutation RemovePromoFromCartMutation {
    removePromoFromCart {
      cart {
        ...CartFields
        grandTotal
      }
      promotion {
        code
        discount
        isValid
        type
      }
    }
  }
  ${cartFragment}
`;

export const useRemovePromoFromCart = () => {
  const [removePromoFromCart, { data, loading, error }] = useMutation(REMOVE_PROMO_FROM_CART_MUTATION);

  return {
    removePromoFromCart,
    loading,
    error,
    cart: get(data, 'removePromoFromCart.cart'),
    warnings: get(data, 'removePromoFromCart.warnings'),
    promotion: get(data, 'removePromoFromCart.promotion'),
  };
};
