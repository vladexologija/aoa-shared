import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';

export const SET_ADDRESS_TO_CART_MUTATION = gql`
  mutation SetAddressToCart($input: SetAddressToCartInput!) {
    setAddressToCart(input: $input) {
      cart {
        id
        subtotal
        discountSubtotal
        grandTotal
        shipping {
          service
          totalCost
          expectedArrival
        }
        taxes {
          amountToCollect
        }
        address {
          id
          fullName
          line1
          city
          state
          zipCode
        }
      }
      warnings
    }
  }
`;

export const useSetAddressToCart = () => {
  const [setAddressToCart, { data, loading, error }] = useMutation(SET_ADDRESS_TO_CART_MUTATION);

  const setAddressToCartCallback = useCallback(
    (addressId) => {
      setAddressToCart({
        variables: {
          input: {
            id: addressId,
          },
        },
      }).catch(() => null);
    },
    [setAddressToCart],
  );

  return {
    setAddressToCart: setAddressToCartCallback,
    loading,
    error,
    cart: get(data, 'setAddressToCart.cart'),
    warnings: get(data, 'setAddressToCart.warnings'),
  };
};
