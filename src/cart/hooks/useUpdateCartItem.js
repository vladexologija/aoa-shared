import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { cartFragment } from 'common/fragments';

export const UPDATE_CART_ITEM_MUTATION = gql`
  mutation UpdateCartItem($input: UpdateCartItemInput!) {
    updateCartItem(input: $input) {
      cart {
        ...CartFields
      }
      warnings
    }
  }
  ${cartFragment}
`;

export const useUpdateCartItem = () => {
  const [updateCartItem, { data, loading, error }] = useMutation(UPDATE_CART_ITEM_MUTATION);

  const updateCartItemCallback = useCallback(
    ({ itemId, quantity, interval = 0 }) => {
      updateCartItem({
        variables: {
          input: {
            id: itemId,
            quantity,
            autoReplenishPlan: interval > 0 ? { interval, intervalType: 'MONTH' } : null,
          },
        },
      }).catch(() => null);
    },
    [updateCartItem],
  );

  return {
    updateCartItem: updateCartItemCallback,
    loading,
    error,
    cart: get(data, 'updateCartItem.cart'),
    warnings: get(data, 'updateCartItem.warnings'),
  };
};
