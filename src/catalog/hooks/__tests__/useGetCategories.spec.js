/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_CATEGORIES_QUERY } from '../useGetCategories';

describe('Test GET_CATEGORIES_QUERY Mutation', () => {
  it('Calling the query with withCollections=true makes a valid query', () => {
    schemaTestValidQuery(GET_CATEGORIES_QUERY, {
      withCollections: true,
    });
  });

  it('Calling the query with withCollections=false makes a valid query', () => {
    schemaTestValidQuery(GET_CATEGORIES_QUERY, {
      withCollections: false,
    });
  });
});
