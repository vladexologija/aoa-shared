/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PRODUCT_QUERY } from '../useGetProduct';

describe('Test GET_CATEGORIES_QUERY Mutation', () => {
  it('Calling the query with id makes a valid query', () => {
    schemaTestValidQuery(GET_PRODUCT_QUERY, {
      id: '10',
    });
  });
});
