/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PRODUCT_TYPES_QUERY } from '../useGetProductTypes';

describe('Test GET_PRODUCT_TYPES_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PRODUCT_TYPES_QUERY);
  });
});
