/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { NOTIFY_WHEN_AVAILABLE_MUTATION } from '../useNotifyWhenAvailable';

describe('Test NOTIFY_WHEN_AVAILABLE_MUTATION Mutation', () => {
  it('Calling the mutation with skuId makes a valid mutation', () => {
    schemaTestValidQuery(NOTIFY_WHEN_AVAILABLE_MUTATION, {
      input: {
        skuId: '10',
      },
    });
  });

  it('Calling the mutation with skuId and email makes a valid mutation', () => {
    schemaTestValidQuery(NOTIFY_WHEN_AVAILABLE_MUTATION, {
      input: {
        skuId: '10',
        email: 'app@aceofair.com',
      },
    });
  });
});
