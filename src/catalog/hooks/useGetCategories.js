import { gql, useQuery, NetworkStatus } from '@apollo/client';

export const GET_CATEGORIES_QUERY = gql`
  query GetCategories($withCollections: Boolean!) {
    getCategories {
      id
      name
      oneLiner
      imageUrl
      collections @include(if: $withCollections) {
        id
        name
        description
        color
        oneLiner
        invertTextColor
        products {
          id
          name
          imageUrl
          comingSoon
          productType {
            id
            name
          }
          skus {
            id
            label
            pricingInformation {
              finalPrice
            }
          }
        }
      }
    }
  }
`;

export const useGetCategories = (withCollections) => {
  const { loading, error, data, refetch, networkStatus } = useQuery(GET_CATEGORIES_QUERY, {
    variables: { withCollections },
    notifyOnNetworkStatusChange: true,
  });

  return {
    loading: loading || networkStatus === NetworkStatus.refetch,
    error,
    data: (data && data.getCategories) || [],
    refetch,
  };
};
