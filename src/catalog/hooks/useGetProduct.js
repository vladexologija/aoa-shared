import { gql, useQuery } from '@apollo/client';

export const GET_PRODUCT_QUERY = gql`
  query GetProduct($id: ID!) {
    getProduct(id: $id) {
      id
      name
      description
      seals {
        id
        imageUrl
      }
      review {
        totalReviews
        score
      }
      whatItIsTitle
      whatItIsText
      whatItIsItems {
        title
        items
      }
      whatItIsQuoteText
      whatItIsQuoteAttribution
      ingredients
      powerBehindTitle
      powerBehindSubtitle
      powerBehindItems {
        title
        text
      }
      whenToUse
      howToUse
      forBestResults
      autoReplenish
      comingSoon
      pictures {
        id
        url
        type
      }
      skus {
        id
        outOfStock
        buyLimitReached
        buyLimitDays
        buyLimit
        contentAmount
        unit
        pricingInformation {
          finalPrice
        }
      }
    }
  }
`;

export const useGetProduct = (id) => {
  const { loading, error, data } = useQuery(GET_PRODUCT_QUERY, {
    variables: { id },
  });

  return {
    loading,
    error,
    data: data && data.getProduct,
  };
};
