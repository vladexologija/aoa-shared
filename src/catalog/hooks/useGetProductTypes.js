import { gql, useQuery } from '@apollo/client';

export const GET_PRODUCT_TYPES_QUERY = gql`
  query GetProductTypes {
    getProductTypes {
      id
      name
      imageUrl
    }
  }
`;

export const useGetProductTypes = () => {
  const { loading, error, data } = useQuery(GET_PRODUCT_TYPES_QUERY);

  return {
    loading,
    error,
    data: (data && data.getProductTypes) || [],
  };
};
