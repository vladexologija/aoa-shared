import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const NOTIFY_WHEN_AVAILABLE_MUTATION = gql`
  mutation NotifyWhenAvailable($input: NotifyWhenAvailableInput!) {
    notifyWhenAvailable(input: $input)
  }
`;

export const useNotifyWhenAvailable = () => {
  const [notifyWhenAvailable, { data, loading, error }] = useMutation(NOTIFY_WHEN_AVAILABLE_MUTATION);

  const notifyWhenAvailableCallback = useCallback(
    ({ skuId, email }) => {
      notifyWhenAvailable({
        variables: {
          input: { skuId, email },
        },
      }).catch(() => null);
    },
    [notifyWhenAvailable],
  );

  return {
    notifyWhenAvailable: notifyWhenAvailableCallback,
    loading,
    error,
    data: data && data.notifyWhenAvailable,
  };
};
