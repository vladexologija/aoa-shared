/* istanbul ignore file */
import { gql } from '@apollo/client';

export const addressFragment = gql`
  fragment AddressFields on Address {
    id
    fullName
    line1
    line2
    city
    state
    zipCode
  }
`;
