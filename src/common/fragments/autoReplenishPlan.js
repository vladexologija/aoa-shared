/* istanbul ignore file */
import { gql } from '@apollo/client';

export const arPlanFragment = gql`
  fragment AutoReplenishPlanFields on AutoReplenishPlan {
    id
    updatable
    pausedAt
    canceledAt
    initialAvailableDate
    desiredDeliveryAt
    lastShipmentAt
    lastDeliveryAt
    lastPaymentDeclinedAt
    orderHasShipped
    orderHasDelivered
    interval
    intervalType
    product {
      id
      name
      imageUrl
    }
    shipping {
      deliveryTrackingURL
    }
  }
`;
