/* istanbul ignore file */
import { gql } from '@apollo/client';

export const cartFragment = gql`
  fragment CartFields on Cart {
    id
    itemsCount
    additionalFees {
      rentalTotal
    }
    subtotal
    discountSubtotal
    items {
      id
      quantity
      available
      autoReplenishPlan {
        interval
        intervalType
      }
      sku {
        id
        label
        outOfStock
        buyLimitReached
        buyLimitDays
        buyLimit
        maxItemsCanBuy
        pricingInformation {
          finalPrice
        }
      }
      product {
        id
        name
        imageUrl
        autoReplenish
      }
    }
  }
`;
