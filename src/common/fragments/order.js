/* istanbul ignore file */
import { gql } from '@apollo/client';

export const orderFragment = gql`
  fragment OrderFields on Order {
    id
    status
    refundable
    cancelable
    canUpdatePaymentMethod
    productsTotal
    taxesTotal
    grandTotal
    rentalTotal
    createdAt
    canceledAt
    deliveredAt
    autoReplenishment
    autoReplenishPlans {
      id
    }
    items {
      id
      quantity
      price
      autoReplenishPlan {
        interval
        intervalType
      }
      sku {
        id
        product {
          id
          name
          imageUrl
        }
      }
    }
    shipping {
      deliveryTrackingCode
      deliveryTrackingURL
      estimatedDeliveryCost
    }
    shippingAddress {
      fullName
      line1
      line2
      city
      state
      zipCode
    }
    paymentMethod {
      id
      brand
      expirationMonth
      expirationYear
      last4Digits
      label
      default
    }
    refundRequest {
      reason
      evaluatedAt
      evaluationNotes
      approved
      requestedAt
      warehouseReceivedAt
      items {
        id
        quantity
        orderItem {
          sku {
            product {
              name
            }
          }
        }
      }
      productsTotal
      rentalTotal
      taxesTotal
      refundTotal
      damagedPackagesTotal
    }
  }
`;
