/* istanbul ignore file */
import { gql } from '@apollo/client';

export const packageFragment = gql`
  fragment PackageFields on CustomerPackage {
    id
    name
    status
    dueDate
    returnedAt
  }
`;
