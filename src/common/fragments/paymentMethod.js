/* istanbul ignore file */
import { gql } from '@apollo/client';

export const paymentMethodFragment = gql`
  fragment PaymentMethodFields on PaymentMethod {
    id
    brand
    expirationMonth
    expirationYear
    last4Digits
    label
  }
`;
