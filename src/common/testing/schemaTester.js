/* eslint-disable */
import EasyGraphQLTester from 'easygraphql-tester';
import fs from 'fs';
import path from 'path';

const schema = fs.readFileSync(path.join(__dirname, '../../../', 'schema.graphql'), 'utf8');
const tester = new EasyGraphQLTester(schema);

const schemaTestQuery = (isValid, query, params) => {
  return tester.test(isValid, query, ...params);
};

export const schemaTestValidQuery = (hook, ...params) => schemaTestQuery(true, hook, params);

export const schemaTestInvalidQuery = (hook, ...params) => schemaTestQuery(false, hook, params);
