import dayjs from 'dayjs';

import { shouldDisableDateWeekend } from '../index';

describe('shared/utils/shouldDisableDateWeekend', () => {
  it('returns false if the date is a week day', () => {
    expect(shouldDisableDateWeekend(dayjs('2019-12-14'))).toBeFalsy();
  });

  it('returns true if the date is a weekend day', () => {
    expect(shouldDisableDateWeekend(dayjs('2019-12-15'))).toBeTruthy();
  });
});
