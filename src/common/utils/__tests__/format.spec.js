import { formatDate, formatPrice, formatCardExpirationDate, formatAutoReplenishPlan, formatCardNumber } from '../index';

describe('utils/format', () => {
  describe('formatDate', () => {
    it('returns the formatted date from a datetime ISO string', () => {
      expect(formatDate('2019-09-04T08:00:00')).toBe('09/04/2019');
      expect(formatDate('2017-01-01T00:00:00')).toBe('01/01/2017');
      expect(formatDate('2018-12-31T23:59:59')).toBe('12/31/2018');
    });
  });

  describe('formatPrice', () => {
    it('returns the formatted price from an integer price in cents', () => {
      const priceInCents = 4299;

      expect(formatPrice(priceInCents)).toBe('$42.99');
    });
  });

  describe('formatCardExpirationDate', () => {
    it('returns the formatted card expiration date with padded month', () => {
      expect(formatCardExpirationDate(2042, 2)).toBe('02/2042');
    });
  });

  describe('formatAutoReplenishPlan', () => {
    it('returns a formatted string showing the correct auto-replenishment data', () => {
      expect(formatAutoReplenishPlan({ interval: 1, intervalType: 'MONTH' })).toBe('Every Month');
      expect(formatAutoReplenishPlan({ interval: 2, intervalType: 'MONTH' })).toBe('Every 2 Months');
      expect(formatAutoReplenishPlan({ interval: 1, intervalType: 'WEEK' })).toBe('Every Week');
      expect(formatAutoReplenishPlan({ interval: 2, intervalType: 'WEEK' })).toBe('Every 2 Weeks');
    });
  });

  describe('formatCardNumber', () => {
    it('returns a formatted string showing the correct card number with pre-padded asterisks', () => {
      expect(formatCardNumber('1234', false)).toBe('**** 1234');
      expect(formatCardNumber('2345', true)).toBe('**** **** **** 2345');
    });
  });
});
