import { isValidPhoneNumber } from '../index';

describe('utils/general', () => {
  describe('isValidPhoneNumber', () => {
    it('returns true for valid US phone numbers', () => {
      expect(isValidPhoneNumber('202-555-0194')).toBe(true);
      expect(isValidPhoneNumber('202 555 0194')).toBe(true);
      expect(isValidPhoneNumber('(202) 555 0194')).toBe(true);
      expect(isValidPhoneNumber('(202) 555-0194')).toBe(true);
      expect(isValidPhoneNumber('(202) 555 - 0194')).toBe(true);
      expect(isValidPhoneNumber('202555-0194')).toBe(true);
      expect(isValidPhoneNumber('2025550194')).toBe(true);
    });

    it('returns false for invalid US phone numbers', () => {
      expect(isValidPhoneNumber('202-555-0190-981')).toBe(false);
      expect(isValidPhoneNumber('202-555-01999')).toBe(false);
      expect(isValidPhoneNumber('202-555-019')).toBe(false);
      expect(isValidPhoneNumber('202555019')).toBe(false);
      expect(isValidPhoneNumber('202 555 019')).toBe(false);
      expect(isValidPhoneNumber('202-555 019')).toBe(false);
      expect(isValidPhoneNumber('+1202')).toBe(false);
      expect(isValidPhoneNumber('202')).toBe(false);
    });
  });
});
