export const shouldDisableDateWeekend = (currentDate) => currentDate.day() === 0;
