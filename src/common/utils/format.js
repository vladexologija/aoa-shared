import capitalize from 'lodash/capitalize';
import dayjs from 'dayjs';
import accounting from 'accounting';

export const formatPrice = (priceInteger, skipDefaultValue = false) => {
  if (priceInteger || priceInteger === 0) {
    return accounting.formatMoney(priceInteger / 100);
  }

  return skipDefaultValue ? priceInteger : '---';
};

export const formatCardExpirationDate = (expirationYear, expirationMonth) => {
  return `${expirationMonth.toString().padStart(2, '0')}/${expirationYear}`;
};

export const DATE_FORMAT = 'MM/DD/YYYY';

export const formatDate = (dateString) => {
  return dayjs(dateString).format(DATE_FORMAT);
};

export const formatAutoReplenishPlan = (arInfo) => {
  let output = 'Every ';

  if (arInfo.interval === 1) {
    output += capitalize(arInfo.intervalType);
  } else {
    output += `${arInfo.interval} ${capitalize(`${arInfo.intervalType}s`)}`;
  }

  return output;
};

export const formatCardNumber = (last4, prePadFull) => {
  let prePadding = '**** ';

  if (prePadFull) {
    prePadding += '**** **** ';
  }

  return `${prePadding}${last4}`;
};
