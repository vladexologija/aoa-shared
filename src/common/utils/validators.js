import { PHONE_REGEX } from './regex';

export const isValidPhoneNumber = (value) => {
  const numberOfDigits = value.match(/\d/g).length;

  return numberOfDigits === 10 && PHONE_REGEX.test(value);
};
