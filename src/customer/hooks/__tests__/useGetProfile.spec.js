/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PROFILE_QUERY } from '../useGetProfile';

describe('Test GET_PROFILE_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PROFILE_QUERY);
  });
});
