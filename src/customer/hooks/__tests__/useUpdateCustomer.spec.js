/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_CUSTOMER_MUTATION } from '../useUpdateCustomer';

describe('Test UPDATE_CUSTOMER_MUTATION Mutation', () => {
  it('Calling the mutation with old/new password makes a valid mutation', () => {
    schemaTestValidQuery(UPDATE_CUSTOMER_MUTATION, {
      input: {
        oldPassword: 'old-pwd',
        newPassword: 'new-pwd',
      },
    });
  });
});
