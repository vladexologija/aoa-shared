import { gql, useQuery, NetworkStatus } from '@apollo/client';

export const GET_PROFILE_QUERY = gql`
  query GetProfile {
    profile {
      email
      firstName
      lastName
    }
  }
`;

export const useGetProfile = () => {
  const { data, error, loading, refetch, networkStatus } = useQuery(GET_PROFILE_QUERY, { notifyOnNetworkStatusChange: true });

  return {
    error,
    loading: loading || networkStatus === NetworkStatus.refetch,
    data: data && data.profile,
    refetch,
  };
};
