import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const UPDATE_CUSTOMER_MUTATION = gql`
  mutation UpdateCustomer($input: UpdateCustomerInput!) {
    updateCustomer(input: $input) {
      id
    }
  }
`;

export const useUpdateCustomer = () => {
  const [updateCustomer, { data, loading, error }] = useMutation(UPDATE_CUSTOMER_MUTATION);

  const updateCustomerCallback = useCallback(
    ({ oldPassword, newPassword }) => {
      updateCustomer({
        variables: {
          input: { oldPassword, newPassword },
        },
      }).catch(() => null);
    },
    [updateCustomer],
  );

  return {
    updateCustomer: updateCustomerCallback,
    loading,
    error,
    data: data && data.updateCustomer,
  };
};
