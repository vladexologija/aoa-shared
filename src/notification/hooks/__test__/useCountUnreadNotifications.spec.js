/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { COUNT_NOTIFICATIONS_QUERY } from '../useCountUnreadNotifications';

describe('Test COUNT_NOTIFICATIONS_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(COUNT_NOTIFICATIONS_QUERY);
  });
});
