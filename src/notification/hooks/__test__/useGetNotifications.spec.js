/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_NOTIFICATIONS } from '../useGetNotifications';

describe('Test GET_NOTIFICATIONS Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_NOTIFICATIONS);
  });
});
