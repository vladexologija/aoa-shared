/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { MARK_READ } from '../useMarkNotificationRead';

describe('Test MARK_READ Mutation', () => {
  it('Calling the mutation with ids parameters makes a valid mutation', () => {
    schemaTestValidQuery(MARK_READ, {
      ids: ['10', '11'],
    });
  });
});
