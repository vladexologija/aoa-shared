import { gql, useLazyQuery } from '@apollo/client';

export const COUNT_NOTIFICATIONS_QUERY = gql`
  query CountUnreadNotifications {
    countUnreadNotifications
  }
`;

export const useCountUnreadNotifications = () => {
  const [countNotifications, { loading, data, error }] = useLazyQuery(COUNT_NOTIFICATIONS_QUERY, {
    fetchPolicy: 'cache-and-network',
  });

  return {
    loading,
    error,
    data: data && data.countUnreadNotifications,
    countNotifications,
  };
};
