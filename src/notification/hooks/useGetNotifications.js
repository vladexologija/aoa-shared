import { gql, useQuery } from '@apollo/client';

export const GET_NOTIFICATIONS = gql`
  query GetNotifications {
    getNotifications {
      id
      message
      readAt
    }
  }
`;

export const useGetNotifications = () => {
  const { loading, error, data } = useQuery(GET_NOTIFICATIONS, {
    fetchPolicy: 'cache-and-network',
  });

  return {
    loading,
    error,
    data: data ? data.getNotifications : [],
  };
};
