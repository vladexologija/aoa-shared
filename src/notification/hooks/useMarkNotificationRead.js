import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const MARK_READ = gql`
  mutation MarkRead($ids: [ID!]!) {
    markNotificationRead(ids: $ids)
  }
`;

export const useMarkNotificationRead = () => {
  const [markRead, { data, loading, error }] = useMutation(MARK_READ);

  const markReadCallback = useCallback(
    (ids) => {
      markRead({
        variables: {
          ids,
        },
      }).catch(() => null);
    },
    [markRead],
  );

  return {
    markRead: markReadCallback,
    loading,
    error,
    data: data && data.markRead,
  };
};
