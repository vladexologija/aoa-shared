/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CANCEL_ORDER_MUTATION } from '../useCancelOrder';

describe('Test CANCEL_ORDER_MUTATION Mutation', () => {
  it('Calling the mutation with id parameters makes a valid mutation', () => {
    schemaTestValidQuery(CANCEL_ORDER_MUTATION, {
      id: '10',
    });
  });
  it('Calling the mutation with id and reason parameters makes a valid mutation', () => {
    schemaTestValidQuery(CANCEL_ORDER_MUTATION, {
      id: '10',
      reason: 'A good reason',
    });
  });
});
