/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_ORDER_QUERY } from '../useGetOrder';

describe('Test GET_ORDER_QUERY Query', () => {
  it('Calling the query with id makes a valid query', () => {
    schemaTestValidQuery(GET_ORDER_QUERY, {
      id: '10',
    });
  });
});
