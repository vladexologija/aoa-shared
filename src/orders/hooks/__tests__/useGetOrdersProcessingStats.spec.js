/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_ORDERS_PROCESSING_STATS } from '../useGetOrdersProcessingStats';

describe('Test GET_ORDERS_PROCESSING_STATS Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_ORDERS_PROCESSING_STATS);
  });
});
