/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PLACED_ORDER_QUERY } from '../useGetPlacedOrder';

describe('Test GET_PLACED_ORDER_QUERY Query', () => {
  it('Calling the query with id makes a valid query', () => {
    schemaTestValidQuery(GET_PLACED_ORDER_QUERY, {
      id: '10',
    });
  });
});
