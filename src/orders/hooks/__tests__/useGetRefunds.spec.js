/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_REFUNDS_QUERY } from '../useGetRefunds';

describe('Test GET_REFUNDS_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_REFUNDS_QUERY);
  });

  it('Calling the query with id makes a valid query', () => {
    schemaTestValidQuery(GET_REFUNDS_QUERY, {
      customerId: '10',
      orderId: '100',
      pendingEvaluation: true,
      pendingReceival: false,
    });
  });
});
