/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { PLACE_ORDER_MUTATION } from '../usePlaceOrder';

describe('Test PLACE_ORDER_MUTATION Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(PLACE_ORDER_MUTATION, {
      input: {
        paymentMethodId: '10',
        primariesToReturnIds: ['100'],
        billingAddressId: '10',
      },
    });
  });
});
