/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { REQUEST_REFUND_MUTATION } from '../useRequestRefund';

describe('Test REQUEST_REFUND_MUTATION Mutation', () => {
  it('Calling the mutation with orderId/items/reason parameters makes a valid mutation', () => {
    schemaTestValidQuery(REQUEST_REFUND_MUTATION, {
      orderId: '10',
      items: [
        {
          orderItemId: '5',
          quantity: 4,
        },
        {
          orderItemId: '6',
          quantity: 2,
        },
      ],
      reason: 'I dont like',
    });
  });
});
