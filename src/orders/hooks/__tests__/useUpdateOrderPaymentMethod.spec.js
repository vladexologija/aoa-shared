/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { UPDATE_ORDER_PAYMENT_METHOD } from '../useUpdateOrderPaymentMethod';

describe('Test SIMULATE_REFUND_MUTATION Mutation', () => {
  it('Calling the mutation with orderId/paymentMethodId parameters makes a valid mutation', () => {
    schemaTestValidQuery(UPDATE_ORDER_PAYMENT_METHOD, {
      input: {
        orderId: '10',
        paymentMethodId: '20',
      },
    });
  });
});
