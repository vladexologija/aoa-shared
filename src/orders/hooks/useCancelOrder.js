import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const CANCEL_ORDER_MUTATION = gql`
  mutation CancelOrder($id: ID!, $reason: String) {
    cancelOrder(id: $id, reason: $reason) {
      id
      refundable
      cancelable
      canUpdatePaymentMethod
      canceledAt
      status
    }
  }
`;

export const useCancelOrder = () => {
  const [cancelOrder, { data, loading, error }] = useMutation(CANCEL_ORDER_MUTATION);

  const cancelOrderCallback = useCallback(
    (id, reason) => {
      return cancelOrder({
        variables: {
          id,
          reason,
        },
      });
    },
    [cancelOrder],
  );

  return {
    cancelOrder: cancelOrderCallback,
    loading,
    error,
    order: data && data.cancelOrder,
  };
};
