import { gql, useQuery } from '@apollo/client';
import { orderFragment } from 'common/fragments';

export const GET_ORDERS_QUERY = gql`
  query GetCustomerOrders {
    getCustomerOrders {
      ...OrderFields
    }
  }
  ${orderFragment}
`;

export const useGetCustomerOrders = () => {
  const { loading, data, error } = useQuery(GET_ORDERS_QUERY, {
    fetchPolicy: 'cache-and-network',
  });

  return {
    loading,
    error,
    orders: (data && data.getCustomerOrders) || [],
  };
};
