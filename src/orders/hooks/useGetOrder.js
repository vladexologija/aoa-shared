import { gql, useQuery } from '@apollo/client';
import { orderFragment } from 'common/fragments';

export const GET_ORDER_QUERY = gql`
  query GetOrder($id: ID!) {
    getOrder(id: $id) {
      ...OrderFields
    }
  }
  ${orderFragment}
`;

export const useGetOrder = (id) => {
  const { loading, data, error } = useQuery(GET_ORDER_QUERY, { variables: { id } });

  return {
    loading,
    error,
    order: data && data.getOrder,
  };
};
