import { gql, useLazyQuery } from '@apollo/client';

export const GET_ORDERS_PROCESSING_STATS = gql`
  query GetOrdersProcessingStats {
    getOrdersProcessingStats {
      waitingForProcessing
      waitingForStock
    }
  }
`;

export const useGetOrdersProcessingStats = () => {
  const [getOrderStats, { loading, data, error }] = useLazyQuery(GET_ORDERS_PROCESSING_STATS, { fetchPolicy: 'cache-and-network' });

  return {
    getOrderStats,
    loading,
    data: data && data.getOrdersProcessingStats,
    error,
  };
};
