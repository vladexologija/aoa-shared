import { gql, useQuery } from '@apollo/client';

export const GET_PLACED_ORDER_QUERY = gql`
  query GetPlacedOrder($id: ID!) {
    getOrder(id: $id) {
      id
    }
  }
`;

export const useGetPlacedOrder = (id) => {
  const { loading, data, error } = useQuery(GET_PLACED_ORDER_QUERY, { variables: { id } });

  return {
    loading,
    error,
    order: data && data.getOrder,
  };
};
