import { gql, useQuery } from '@apollo/client';

export const GET_REFUNDS_QUERY = gql`
  query GetRefunds($filters: RefundFiltersInput) {
    getRefunds(filters: $filters) {
      id
      evaluatedAt
      evaluationNotes
      approved
      warehouseReceivedAt
      requestedAt
      items {
        orderItem {
          id
          sku {
            product {
              name
            }
          }
        }
        quantity
        receivedQuantity
      }
    }
  }
`;

export const useGetRefunds = (filters) => {
  const { loading, error, data } = useQuery(GET_REFUNDS_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: { filters },
  });

  return {
    loading,
    error,
    data: (data && data.getRefunds) || [],
  };
};
