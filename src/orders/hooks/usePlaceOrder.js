import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';
import { GET_CART_QUERY } from 'cart/hooks';
import { GET_ORDERS_QUERY } from './useGetCustomerOrders';

export const PLACE_ORDER_MUTATION = gql`
  mutation PlaceOrder($input: PlaceOrderInput!) {
    placeOrder(input: $input) {
      id
    }
  }
`;

export const usePlaceOrder = () => {
  const [placeOrder, { data, loading, error }] = useMutation(PLACE_ORDER_MUTATION, {
    refetchQueries: [
      {
        query: GET_ORDERS_QUERY,
      },
    ],
    update(cache, { data: mutationResult }) {
      if (get(mutationResult, 'placeOrder.id')) {
        cache.writeQuery({
          query: GET_CART_QUERY,
          data: { getCart: null },
        });

        cache.gc();
      }
    },
  });

  const placeOrderCallback = useCallback(
    ({ paymentMethodId, primariesToReturnIds, billingAddressId }) => {
      placeOrder({
        variables: {
          input: {
            paymentMethodId,
            primariesToReturnIds,
            billingAddressId,
          },
        },
      }).catch(() => null);
    },
    [placeOrder],
  );

  return {
    placeOrder: placeOrderCallback,
    loading,
    error,
    order: get(data, 'placeOrder'),
  };
};
