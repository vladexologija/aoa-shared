import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const REQUEST_REFUND_MUTATION = gql`
  mutation RequestRefund($orderId: ID!, $items: [RefundItemInput]!, $reason: String!) {
    requestRefund(orderId: $orderId, items: $items, reason: $reason) {
      id
      order {
        id
        refundable
        status
      }
    }
  }
`;

export const useRequestRefund = () => {
  const [requestRefund, { data, loading, error }] = useMutation(REQUEST_REFUND_MUTATION);

  const requestRefundCallback = useCallback(
    (orderId, items, reason) => {
      requestRefund({
        variables: {
          orderId,
          items,
          reason,
        },
      }).catch(() => null);
    },
    [requestRefund],
  );

  return {
    requestRefund: requestRefundCallback,
    loading,
    error,
    data: data && data.requestRefund,
  };
};
