import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const SIMULATE_REFUND_MUTATION = gql`
  mutation simulateRefund($orderId: ID!, $items: [RefundItemInput]!) {
    simulateRefund(orderId: $orderId, items: $items) {
      items {
        product {
          name
        }
        id
        quantity
        price
      }
      rentalTotal
      taxesTotal
      refundTotal
    }
  }
`;

export const useSimulateRefund = () => {
  const [simulateRefund, { data, loading, error }] = useMutation(SIMULATE_REFUND_MUTATION);

  const simulateRefundCallback = useCallback(
    (orderId, items) => {
      simulateRefund({
        variables: {
          orderId,
          items,
        },
      }).catch(() => null);
    },
    [simulateRefund],
  );

  return {
    simulateRefund: simulateRefundCallback,
    loading,
    error,
    data: data && data.simulateRefund,
  };
};
