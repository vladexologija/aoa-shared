import { gql, useMutation } from '@apollo/client';
import { useCallback } from 'react';

export const UPDATE_ORDER_PAYMENT_METHOD = gql`
  mutation UpdateOrderPaymentMethod($input: UpdateOrderPaymentInput!) {
    updateOrderPaymentMethod(input: $input) {
      id
      paymentCaptureFailedAt
      status
      canUpdatePaymentMethod
      paymentMethod {
        id
        brand
        expirationMonth
        expirationYear
        last4Digits
        label
        default
      }
    }
  }
`;

export const useUpdateOrderPaymentMethod = () => {
  const [updateOrderPaymentMethodMutation, { data, loading, error }] = useMutation(UPDATE_ORDER_PAYMENT_METHOD);

  const updateOrderPaymentMethod = useCallback(
    (orderId, paymentMethodId) =>
      updateOrderPaymentMethodMutation({
        variables: {
          input: {
            orderId,
            paymentMethodId,
          },
        },
      }).catch(() => null),
    [updateOrderPaymentMethodMutation],
  );

  return {
    error,
    loading,
    updateOrderPaymentMethod,
    order: data && data.updateOrderPaymentMethod,
  };
};
