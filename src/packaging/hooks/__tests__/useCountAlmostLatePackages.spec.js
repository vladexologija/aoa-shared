/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { COUNT_ALMOST_LATE_PACKAGES_QUERY } from '../useCountAlmostLatePackages';

describe('Test SIMULATE_REFUND_MUTATION Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(COUNT_ALMOST_LATE_PACKAGES_QUERY);
  });
});
