/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PACKAGING_QUERY } from '../useGetMyPackaging';

describe('Test GET_PACKAGING_QUERY Query', () => {
  it('Calling the query with customerId/inUse parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PACKAGING_QUERY, {
      customerId: '100',
      inUse: false,
    });
  });

  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PACKAGING_QUERY);
  });
});
