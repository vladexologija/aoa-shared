/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PACKAGE_TYPE_QUERY } from '../useGetPackageType';

describe('Test GET_PACKAGE_TYPE_QUERY Query', () => {
  it('Calling the query with customerId/inUse parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PACKAGE_TYPE_QUERY, {
      id: '100',
    });
  });
});
