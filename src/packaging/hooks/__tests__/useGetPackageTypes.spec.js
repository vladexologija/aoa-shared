/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PACKAGE_TYPES_QUERY } from '../useGetPackageTypes';

describe('Test GET_PACKAGE_TYPES_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PACKAGE_TYPES_QUERY);
  });
});
