/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PACKAGING_USED_QUERY } from '../useGetPackagingUsed';

describe('Test GET_PACKAGING_USED_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PACKAGING_USED_QUERY);
  });
});
