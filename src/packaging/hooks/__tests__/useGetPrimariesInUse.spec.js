/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PRIMARIES_QUERY } from '../useGetPrimariesInUse';

describe('Test GET_PRIMARIES_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PRIMARIES_QUERY);
  });
});
