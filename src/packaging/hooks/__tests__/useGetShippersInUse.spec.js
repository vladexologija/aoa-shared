/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_SHIPPERS_QUERY } from '../useGetShippersInUse';

describe('Test GET_SHIPPERS_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_SHIPPERS_QUERY);
  });
});
