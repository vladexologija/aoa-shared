/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { REPRINT_RETURN_LABEL_MUTATION } from '../useReprintReturnLabel';

describe('Test REPRINT_RETURN_LABEL_MUTATION Mutation', () => {
  it('Calling the mutation with id makes a valid mutation', () => {
    schemaTestValidQuery(REPRINT_RETURN_LABEL_MUTATION, {
      id: '10',
    });
  });
});
