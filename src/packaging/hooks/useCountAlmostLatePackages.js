import { gql, useQuery } from '@apollo/client';

export const COUNT_ALMOST_LATE_PACKAGES_QUERY = gql`
  query CountAlmostLatePackages {
    countPackagesCloseToDueDate
  }
`;

export const useCountAlmostLatePackages = () => {
  const { loading, error, data } = useQuery(COUNT_ALMOST_LATE_PACKAGES_QUERY);

  return {
    loading,
    error,
    closeToDueDateCount: (data && data.countPackagesCloseToDueDate) || 0,
  };
};
