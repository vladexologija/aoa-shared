import { gql, useQuery } from '@apollo/client';
import get from 'lodash/get';
import { packageFragment } from 'common/fragments';

export const GET_PACKAGING_QUERY = gql`
  query GetMyPackaging($customerId: ID, $inUse: Boolean) {
    getMyPackaging(inUse: $inUse, customerId: $customerId) {
      shippers {
        ...PackageFields
      }
      primaries {
        ...PackageFields
        product {
          name
          imageUrl
        }
      }
    }
  }
  ${packageFragment}
`;

export const useGetMyPackaging = ({ inUse, customerId }) => {
  const { loading, error, data, refetch } = useQuery(GET_PACKAGING_QUERY, {
    variables: { inUse, customerId },
  });

  return {
    refetch,
    loading,
    error,
    primaries: get(data, 'getMyPackaging.primaries', []),
    shippers: get(data, 'getMyPackaging.shippers', []),
  };
};
