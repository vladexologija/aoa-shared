import { gql, useQuery } from '@apollo/client';

export const GET_PACKAGE_TYPE_QUERY = gql`
  query GetPackageType($id: ID!) {
    getPackageType(id: $id) {
      id
      name
    }
  }
`;

export const useGetPackageType = (id) => {
  const { loading, error, data } = useQuery(GET_PACKAGE_TYPE_QUERY, {
    variables: { id },
  });

  return {
    loading,
    error,
    data: data && data.getPackageType,
  };
};
