import { gql, useQuery } from '@apollo/client';
import get from 'lodash/get';

export const GET_PACKAGE_TYPES_QUERY = gql`
  query GetPackageTypes {
    getPackageTypes {
      shippers {
        id
        name
        label
      }
      primaries {
        id
        name
        label
      }
    }
  }
`;

export const useGetPackageTypes = () => {
  const { data, error, loading } = useQuery(GET_PACKAGE_TYPES_QUERY);

  return {
    error,
    loading,
    shippers: get(data, 'getPackageTypes.shippers', []),
    primaries: get(data, 'getPackageTypes.primaries', []),
  };
};
