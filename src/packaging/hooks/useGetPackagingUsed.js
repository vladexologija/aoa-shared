import { gql, useQuery } from '@apollo/client';

export const GET_PACKAGING_USED_QUERY = gql`
  query GetPackagingUsed {
    packagingUsed {
      id
      returnedAt
      status
      name
      packageType {
        name
        isShipper
      }
      product {
        id
        name
        imageUrl
      }
    }
  }
`;

export const useGetPackagingUsed = () => {
  const { loading, error, data } = useQuery(GET_PACKAGING_USED_QUERY);

  return {
    loading,
    error,
    packaging: (data && data.packagingUsed) || [],
  };
};
