import { gql, useQuery } from '@apollo/client';

export const GET_PRIMARIES_QUERY = gql`
  query GetPrimaries {
    primariesInUse {
      id
      dueDate
      status
      product {
        id
        name
        imageUrl
      }
    }
  }
`;

export const useGetPrimariesInUse = () => {
  const { loading, error, data } = useQuery(GET_PRIMARIES_QUERY);

  return {
    loading,
    error,
    primaries: (data && data.primariesInUse) || [],
  };
};
