import { gql, useQuery } from '@apollo/client';

export const GET_SHIPPERS_QUERY = gql`
  query GetShippers {
    shippersInUse {
      id
      dueDate
      status
      name
    }
  }
`;

export const useGetShippersInUse = () => {
  const { loading, error, data } = useQuery(GET_SHIPPERS_QUERY);

  return {
    loading,
    error,
    shippers: (data && data.shippersInUse) || [],
  };
};
