import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const REPRINT_RETURN_LABEL_MUTATION = gql`
  mutation reprintReturnLabel($id: ID!) {
    reprintReturnLabel(customerPackageId: $id)
  }
`;

export const useReprintReturnLabel = () => {
  const [reprintReturnLabelMutation, { data, loading, error }] = useMutation(REPRINT_RETURN_LABEL_MUTATION);

  const reprintReturnLabel = useCallback(
    (id) => {
      reprintReturnLabelMutation({
        variables: { id },
      }).catch(() => null);
    },
    [reprintReturnLabelMutation],
  );

  return {
    reprintReturnLabel,
    loading,
    error,
    returnLabelUrl: data,
  };
};
