/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CREATE_PAYMENT_SETUP_INTENT_MUTATION } from '../useCreatePaymentSetupIntent';

describe('Test CREATE_PAYMENT_SETUP_INTENT_MUTATION Mutation', () => {
  it('Calling the mutation without parameters makes a valid mutation', () => {
    schemaTestValidQuery(CREATE_PAYMENT_SETUP_INTENT_MUTATION);
  });
});
