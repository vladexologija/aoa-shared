/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { DELETE_PAYMENT_METHOD_MUTATION } from '../useDeletePaymentMethod';

describe('Test DELETE_PAYMENT_METHOD_MUTATION Mutation', () => {
  it('Calling the mutation with id makes a valid mutation', () => {
    schemaTestValidQuery(DELETE_PAYMENT_METHOD_MUTATION, {
      id: '10',
    });
  });
});
