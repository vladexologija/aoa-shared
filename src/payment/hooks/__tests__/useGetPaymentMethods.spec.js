/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PAYMENT_METHODS_QUERY } from '../useGetPaymentMethods';

describe('Test GET_PAYMENT_METHODS_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PAYMENT_METHODS_QUERY);
  });

  it('Calling the query with customer id makes a valid query', () => {
    schemaTestValidQuery(GET_PAYMENT_METHODS_QUERY, {
      customerId: '10',
    });
  });
});
