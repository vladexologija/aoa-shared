/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { SAVE_PAYMENT_METHOD_MUTATION } from '../useSavePaymentMethod';

describe('Test SAVE_PAYMENT_METHOD_MUTATION Mutation', () => {
  it('Calling the mutation with parameters makes a valid mutation', () => {
    schemaTestValidQuery(SAVE_PAYMENT_METHOD_MUTATION, {
      paymentMethod: {
        id: '10',
        setupIntentId: '100',
        default: true,
        label: 'main',
      },
    });
  });
});
