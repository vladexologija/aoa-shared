import { gql, useMutation } from '@apollo/client';

export const CREATE_PAYMENT_SETUP_INTENT_MUTATION = gql`
  mutation createPaymentSetupIntent {
    createPaymentSetupIntent {
      id
      clientSecret
    }
  }
`;

export const useCreatePaymentSetupIntent = () => {
  const [createPaymentSetupIntent, { data, loading, error }] = useMutation(CREATE_PAYMENT_SETUP_INTENT_MUTATION);

  return {
    createPaymentSetupIntent,
    loading,
    error,
    data: data && data.createPaymentSetupIntent,
  };
};
