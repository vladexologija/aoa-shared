import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import { GET_PAYMENT_METHODS_QUERY } from './useGetPaymentMethods';

export const DELETE_PAYMENT_METHOD_MUTATION = gql`
  mutation deletePaymentMethod($id: ID!) {
    deletePaymentMethod(id: $id)
  }
`;

export const useDeletePaymentMethod = () => {
  const [deletePaymentMethod, { data, loading, error }] = useMutation(DELETE_PAYMENT_METHOD_MUTATION, {
    refetchQueries: [
      {
        query: GET_PAYMENT_METHODS_QUERY,
      },
    ],
  });

  const deletePaymentMethodCallback = useCallback(
    (id) => {
      deletePaymentMethod({
        variables: {
          id,
        },
      }).catch(() => null);
    },
    [deletePaymentMethod],
  );

  return {
    deletePaymentMethod: deletePaymentMethodCallback,
    loading,
    error,
    data,
  };
};
