import { gql, useQuery, NetworkStatus } from '@apollo/client';
import { paymentMethodFragment } from 'common/fragments';

export const GET_PAYMENT_METHODS_QUERY = gql`
  query GetPaymentMethodsQuery($customerId: ID) {
    getPaymentMethods(customerId: $customerId) {
      ...PaymentMethodFields
      default
      hasAutoReplenishPlans
    }
  }
  ${paymentMethodFragment}
`;

export const useGetPaymentMethods = (customerId) => {
  const { loading, data, error, networkStatus, refetch } = useQuery(GET_PAYMENT_METHODS_QUERY, {
    notifyOnNetworkStatusChange: true,
    variables: { customerId },
  });

  return {
    loading: loading || networkStatus === NetworkStatus.refetch,
    error,
    paymentMethods: (data && data.getPaymentMethods) || [],
    refetch,
  };
};
