import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import { paymentMethodFragment } from 'common/fragments';
import { GET_PAYMENT_METHODS_QUERY } from './useGetPaymentMethods';

export const SAVE_PAYMENT_METHOD_MUTATION = gql`
  mutation SavePaymentMethod($paymentMethod: PaymentMethodInput!) {
    savePaymentMethod(paymentMethod: $paymentMethod) {
      ...PaymentMethodFields
      default
      hasAutoReplenishPlans
    }
  }
  ${paymentMethodFragment}
`;

export const useSavePaymentMethod = () => {
  const [savePaymentMethod, { data, loading, error }] = useMutation(SAVE_PAYMENT_METHOD_MUTATION, {
    refetchQueries: [
      {
        query: GET_PAYMENT_METHODS_QUERY,
      },
    ],
  });

  const savePaymentMethodCallback = useCallback(
    (paymentMethod) => {
      savePaymentMethod({
        variables: {
          paymentMethod,
        },
      }).catch(() => null);
    },
    [savePaymentMethod],
  );

  return {
    savePaymentMethod: savePaymentMethodCallback,
    loading,
    error,
    data,
  };
};
