/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { ANSWER_QUIZ_MUTATION } from '../useAnswerQuiz';

describe('Test ANSWER_QUIZ_MUTATION Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(ANSWER_QUIZ_MUTATION, {
      input: {
        id: '10',
        responses: [
          { questionId: '100', response: true },
          { questionId: '101', response: false },
        ],
      },
    });
  });
});
