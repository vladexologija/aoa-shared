/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_CURRENT_QUIZ_QUERY } from '../useGetCurrentQuiz';

describe('Test GET_CURRENT_QUIZ_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_CURRENT_QUIZ_QUERY);
  });
});
