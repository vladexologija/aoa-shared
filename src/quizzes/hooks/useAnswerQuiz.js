import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

export const ANSWER_QUIZ_MUTATION = gql`
  mutation AnswerQuiz($input: AnswerQuizInput!) {
    answerQuiz(input: $input) {
      id
      planetScore
      balance
    }
  }
`;

export const useAnswerQuiz = () => {
  const [answerQuiz, { data, loading, error }] = useMutation(ANSWER_QUIZ_MUTATION);

  const answerQuizCallback = useCallback(
    ({ id, responses }) => {
      answerQuiz({
        variables: {
          input: {
            id,
            responses,
          },
        },
      }).catch(() => null);
    },
    [answerQuiz],
  );

  return {
    answerQuiz: answerQuizCallback,
    loading,
    error,
    data: data && data.answerQuiz,
  };
};
