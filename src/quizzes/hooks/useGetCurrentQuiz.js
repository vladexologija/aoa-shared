import { gql, useQuery } from '@apollo/client';

export const GET_CURRENT_QUIZ_QUERY = gql`
  query GetCurrentQuiz {
    currentQuiz {
      id
      name
      description
      questions {
        id
        content
        answer
      }
    }
  }
`;

export const useGetCurrentQuiz = () => {
  const { data, error, loading } = useQuery(GET_CURRENT_QUIZ_QUERY);

  return {
    error,
    loading,
    currentQuiz: data && data.currentQuiz,
  };
};
