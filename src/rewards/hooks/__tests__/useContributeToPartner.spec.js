/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { CONTRIBUTE_TO_PARTNER_MUTATION } from '../useContributeToPartner';

describe('Test CONTRIBUTE_TO_PARTNER_MUTATION Mutation', () => {
  it('Calling the mutation id/amount parameters makes a valid mutation', () => {
    schemaTestValidQuery(CONTRIBUTE_TO_PARTNER_MUTATION, {
      input: {
        id: '10',
        amount: 10,
      },
    });
  });
});
