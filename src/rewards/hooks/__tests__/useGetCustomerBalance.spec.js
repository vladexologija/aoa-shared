/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_CUSTOMER_BALANCE_QUERY } from '../useGetCustomerBalance';

describe('Test GET_CUSTOMER_BALANCE_QUERY Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_CUSTOMER_BALANCE_QUERY);
  });
});
