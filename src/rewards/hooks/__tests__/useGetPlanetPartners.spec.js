/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PARTNERS_QUERY } from '../useGetPlanetPartners';

describe('Test GET_PARTNERS_QUERY Mutation', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PARTNERS_QUERY);
  });
});
