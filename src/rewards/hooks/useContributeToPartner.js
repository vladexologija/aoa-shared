import { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';
import get from 'lodash/get';

export const CONTRIBUTE_TO_PARTNER_MUTATION = gql`
  mutation ContributeToPartner($input: ContributeInput!) {
    contributeToPartner(input: $input) {
      transferSucceed
      customerBalance {
        id
        balance
      }
      partner {
        id
        allTimeDonations
        thisYearDonations
      }
    }
  }
`;

export const useContributeToPartner = () => {
  const [contributeToPartner, { data, loading, error }] = useMutation(CONTRIBUTE_TO_PARTNER_MUTATION);

  const contributeToPartnerCallback = useCallback(
    (id, amount) => {
      contributeToPartner({
        variables: {
          input: {
            id,
            amount,
          },
        },
      }).catch(() => null);
    },
    [contributeToPartner],
  );

  return {
    contributeToPartner: contributeToPartnerCallback,
    loading,
    error,
    transferSucceed: get(data, 'contributeToPartner.transferSucceed'),
  };
};
