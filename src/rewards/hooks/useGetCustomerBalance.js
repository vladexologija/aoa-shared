import { gql, useQuery } from '@apollo/client';
import get from 'lodash/get';

export const GET_CUSTOMER_BALANCE_QUERY = gql`
  query GetCustomerBalance {
    getCustomerBalance {
      id
      balance
      planetScore
    }
  }
`;

export const useGetCustomerBalance = () => {
  const { data, error, loading } = useQuery(GET_CUSTOMER_BALANCE_QUERY);

  return {
    error,
    loading,
    balance: get(data, 'getCustomerBalance.balance', 0),
    planetScore: get(data, 'getCustomerBalance.planetScore', ''),
  };
};
