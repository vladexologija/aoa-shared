import { gql, useQuery } from '@apollo/client';

export const GET_PARTNERS_QUERY = gql`
  query GetPlanetPartners {
    getCharities {
      id
      name
      description
      website
      imageUrl
      allTimeDonations
      thisYearDonations
    }
  }
`;

export const useGetPlanetPartners = () => {
  const { data, error, loading, refetch } = useQuery(GET_PARTNERS_QUERY);

  return {
    error,
    loading,
    partners: (data && data.getCharities) || [],
    refetch,
  };
};
