/* eslint-disable jest/expect-expect */
import { schemaTestValidQuery } from 'common/testing';
import { GET_PUBLIC_SETTINGS_QUERY } from '../useGetPublicSettings';

describe('Test GET_PUBLIC_SETTINGS_QUERY Query', () => {
  it('Calling the query without parameters makes a valid query', () => {
    schemaTestValidQuery(GET_PUBLIC_SETTINGS_QUERY);
  });
});
