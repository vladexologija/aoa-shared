import { gql, useQuery } from '@apollo/client';

export const GET_PUBLIC_SETTINGS_QUERY = gql`
  query GetPublicSettings {
    publicSettings {
      name
      value
    }
  }
`;

export const useGetPublicSettings = () => {
  const { data, error, loading } = useQuery(GET_PUBLIC_SETTINGS_QUERY);

  let preOrderMode;
  let contactEmail;
  let contactPhone;

  ((data && data.publicSettings) || []).forEach(({ name, value }) => {
    switch (name) {
      case 'CONTACT_EMAIL':
        contactEmail = value;
        break;
      case 'CONTACT_PHONE':
        contactPhone = value;
        break;
      case 'PRE_ORDER_MODE':
        preOrderMode = value === 'true';
        break;
      default:
        break;
    }
  });

  return {
    error,
    loading,
    preOrderMode,
    contactEmail,
    contactPhone,
  };
};
